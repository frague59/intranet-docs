# Documentation du canal pour les éditeurs et les utilisateurs

<http://intranet.docs.tourcoing.fr/intranet-docs/>

## Édition de la documentation

### Cloner le dépot (ssh)

#### SSH

```shell
$ git clone git@gitlab.ville.tg:intranet/intranet-docs.git
$ cd intranet-docs
$ git checkout develop
```

#### HTTPS

```shell
$ git clone https://gitlab.ville.tg/intranet/intranet-docs.git
$ cd intranet-docs
$ git checkout develop
```

### Installer les dépendances

#### Python

+ Avec python directement

```shell
$ python -m venv --prompt . ./.venv
$ . .venv/bin/activate
$ pip install -r requirements.txt
```

+ Avec [uv](https://docs.astral.sh/uv/)

```shell
$ uv venv -m venv --seed --prompt .
$ . .venv/bin/activate
$ uv pip install -r requirements.txt
```

#### JS
```shell
$ npm i
```

### Éditer la documentation

... avec [VSCodium](https://vscodium.com/) ...

Il est possible de visualiser directement les modifications en utilisant le mode `live` de [mkdocs](https://www.mkdocs.org/)

```shell
$ mkdocs serve
```

et aller à l'adresse du serveur de développement : <http://127.0.0.1:5000/>
et aller à l'adresse du serveur de développement : <http://127.0.0.1:5000/>

### Publier la documentation

La publication de la documentation est automatisée par gitlab.

Pour vérifier le bon fonctionnement : <https://gitlab.ville.tg/intranet/intranet-docs/-/pipelines>
