---
title: Accompagnements
summary: Administration des accompagnements
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les accompagnements sont données à titre indicatif.

## Gestion des accompagnements

La gestion des accompagnements est comparable à celle des [plats](Plats.md).
