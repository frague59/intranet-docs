---
title: Plats
summary: Administration des plats du restaurant municipal
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les plats sont les éléments centraux des menus, ils permettent notamment de fixe le tarif qui sera appliqué
aux convives.

## Gestion des plats

Dans l'administration du canal, cliquer sur le bouton "Restauration"
puis "Plats"

![Sélectionner l'administration des plats](images/img-plats-01.png)

Une liste de plats s'affiche, avec des prix "préférentiels" à appliquer à ces plats.

![Liste des plats](images/img-plats-02.png)

Les plats sont par ordre alphabétique, il est possible de les filtrer par nom (en utilisant la recherche en haut de la
page) et par prix préférentiel (en utilisant le filtre latéral.

## Modifier un plat existant

Pour modifier un plat existant, il suffit de cliquer sur son nom dans la liste.

![Modification d'un plat](images/img-plats-03.png)

Il est alors possible de modifier le nom du plat, et son prix préférentiel.

!!! warning "Attention"

    Il est impossible de renommer un plat si un autre a déjà le même nom !

## Ajouter un plat

Pour ajouter un plat, il faut cliquer sur le bouton "Ajouter un plat"
en haut à droite de la page.

![Ajouter un plat](images/img-plats-04.png)

Il faut saisir le nom du nouveau plat, et indiquer le prix préférentiel.

!!! warning "Attention"

    Il est impossible de créer un plat si un autre a déjà le même nom !

## Supprimer une plat

Pour supprimer un plat, 2 possibilités :

+ Depuis la liste des plats, cocher le case à cocher à gauche du nom du plat, puis cliquer sur "Supprimer" en bas de la
  page.
  ![Suppression d'un plat depuis la liste](images/img-plats-05.png)
+ Depuis la vue de détail du plat, cliquer sur la flêche à droite du bouton "Enregistrer", un menu s'ouvre avec l'item "
  Supprimer"
  ![Suppression depuis la vue de détail du plat](images/img-plats-06.png)

!!! note

    Dans les deux cas, une page intermédiaire permet de valider la suppression effective du plat.**

    <figure markdown="span">
      ![Page de confirmation de la suppression](images/img-plats-07.png)
      <figcaption>Page de confirmation de la suppression</figcaption>
    </figure>
