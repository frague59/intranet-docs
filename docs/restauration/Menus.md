---
title: Menus
summary: Administration des menus du restaurant municipal
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les menus sont un ensemble de plats et d'accompagnements, pour un jour donné. Il est possible d'indiquer qu'un menu
est "spécial" (pour une occasion spécifique : "Noël", "Nouvel an chinois"...)

Ils sont affichés depuis l'icône ![Icône du menu](images/img-menus-01.png) présente dans
les [Applications](../publications/ApplicationLinks.md).

Le "menu du jour" est affiché jusqu'à 14:00, ensuite c'est le menu du jour suivant qui est affiché.

## Lister les menus

Pour accéder aux menus, depuis l'interface d'administration du canal, cliquer sur **Restauration** puis **Menus**.

Les menus sont listés par ordre de date croissante. Il est possible de filtrer les menus selon qu'ils sont spéciaux ou
que le restaurant est fermé.

### Ajouter un nouveau menu

Pour ajouter un nouveau menu, cliquer sur le bouton **Ajouter un menu du restaurant** sur la page de liste des menus.

![Ajouter un menu](images/img-menus-02.png)

Les champs sont les suivants :

| Champ                                | Type                                    | Description                       |
|--------------------------------------|-----------------------------------------|-----------------------------------|
| Date                                 | date                                    | Date du menu                      |
| Menu spécial                         | Booléen (**Vrai**, **Faux**)            | Est-ce un menu spécial ?          |
| Nom du menu                          | Texte                                   | Titre du menu spécial             |
| Le restaurant est fermé              | Booléen (**Vrai**, **Faux**)            | Le restaurant est fermé           |
| Raison de la fermeture du restaurant | [Texte enrichi](../general/RichText.md) | Raison de fermeture du restaurant |
| Plats                                | Liste de plats                          | Voir ci-dessous                   |
| Accompagnements                      | Liste d'accompagnements                 | Voir ci-dessous                   |

+ **Plats**
  Il est possible de sélectionner les plats dans la liste. Un moteur de recherche permet de rechercher les plats par
  leur nom. On peut ajouter autant de plats que nécessaire. Vous pouvez préciser le prix de ce plat.

+ **Accompagnements**
  Il est possible d'ajouter autant d'accompagnements que nécessaire. La recherche est activée en tapant une partie du
  nom de l'accompagnement.

Cliquer sur "**Enregistrer**" pour valider le menu.

### Modifier un menu

Pour modifier un menu, il faut cliquer sur la date dans la liste des menus. Il est alors possible d'éditer le menu,
pour indiquer une fermeture par exemple.
