---
title: Menu du restaurant municipal
summary: Administrer le menu du restaurant municipal
authors:
  - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2023-04-12
---

Le menu du restaurant municipal est un élément important du canal.

Son administration simplifiée permet aux agents d'ajouter des menus simplement.

+ [Plats](./Plats.md)
+ [Accompagnements](./Accompagnements.md)
+ [Menus](./Menus.md)
