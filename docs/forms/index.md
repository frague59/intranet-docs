---
title: Formulaires pour les utilisateurs
summary: Gestion des formulaires dans l'adminnistration
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-24
---

Les formulaires permettent de mettre en place des questionnaires ou des systèmes d'inscriptions.

Un formulaire se présente sous la forme d'une page, qui peut être insérée dans l'arborescence du site.

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

## Créer un formulaire

Pour créer une formulaire, aller dans la page ârent du formulaire, cliquer sur "**Ajouter une sous-page**", puis
sélectionner "**Page du formulaire**".

<figure markdown="span">

![Sélectionner de type de page Formulaire](images/img-forms-01.png)
  <figcaption>Sélectionner de type de page "Formulaire"</figcaption>
</figure>

<figure markdown="span">

![Page de création d'un formulaire](images/img-forms-02.png)
  <figcaption>Page de création d'un formulaire</figcaption>
</figure>

### Champs de la page de formulaire

| Champ                                  | Type                                                       | Description                                                                                      |
|----------------------------------------|------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| Titre                                  | Texte libre (**obligatoire**)                              | Titre de la page                                                                                 |
| Chapeau                                | [Texte enrichi](../general/RichText.md)                    | Chapeau du questionnaire                                                                         |
| Public de la page                      | Choix unique (**obligatoire**)                             | Parmi (**Mairie**, **C.C.A.S** et **Mairie et C.C.A.S**)                                         |
| Vignette                               | [Image](../general/Images.md) (**obligatoire**)            | Vignette carrée, affichée dans les listes de pages                                               |
| Bannière                               | [Image](../general/Images.md)                              | Bannière, affichée en haut de la page                                                            |
| Champs de formulaire                   | Liste de [champs de formulaire](#les-champs-de-formulaire) | Liste des champs                                                                                 |
| Texte de remerciement                  | [Texte enrichi](../general/RichText.md)                    | Texte re remerciement affiché dans la page finale du formulaire, apès enregistrement.            |
| Envoyer une courriel aux organisateurs | Booléen                                                    | Envoie un courriel au moment de l'inscription aux organisateurs                                  |
| Envoyer une courriel aux demandeurs    | Booléen                                                    | Envoie un courriel au moment de l'inscription au demandeur                                       |
| Adresse expéditeur                     | Adresse électronique                                       | Adresse électronique qui effectue l'envoi des courriels. Par défaut : <lecanal@ville-tourcoing.fr> |
| Adresse destinataire                   | Adresse électronique                                       | Adresse supplémentaire où envoyer les soumissions de formulaire                                  |
| Sujet                                  | Texte libre                                                | Sujet du courriel envoyé                                                                         |

### Les champs de formulaire

!!! note

    Il est possible d'**ordonner** les champs du formulaire en utilisant les boutions adhoc en haut de la définition du
    champ. Il est également possible de **supprimer** un champ.

    <figure markdown="span">

      ![Bouton de modification des l'ordre des champs](images/img-forms-03.png)
      <figcaption>Bouton de modification des l'ordre des champs</figcaption>
    </figure>

+ Texte d'une seule ligne
+ Texte multiligne
+ Adresse électronique
+ Nombre
+ URL
+ Case à cocher – :custom-check-square:
+ Cases à cocher – :custom-check-square:
+ Menu déroulant – Sélection unique
+ Sélection multiple
+ Boutons radio (choix unique dans une liste d'options affichées)
+ Date
+ Date/heure
+ Champ caché
+ [Bouton radio avec des limites](#bouton-radio-avec-des-limites)
+ [Direction / service](#direction--service)
+ [Champ avec une expression rationnelle](#champ-avec-une-expression-rationnelle)
+ Numéro de téléphone
+ Nom complet

### Bouton radio avec des limites

Les boutons radio avec limite permettent de proposer des choses en nombre limitées, par exemple des créneaux de visites
aux musées de Tourcoing.

Pour les utiliser, le format des choix est sous la forme suivante :

```text
<choix 1>:10,<choix 2>:20,<choix 3>:10
```

!!! warning "Attention"

    Il faut être attentif à ne pas utiliser les caractères `,` et `:` à l'intérieur des intitulés des options !<br>
    Si c'est le cas, une erreur sera émise par l'application.

Où `<choix 1>` est ce qui sera affiché dans la liste de sélection, et `10` est le nombre total de places disponibles
pour ce choix.

L'application affichera automatiquement les choix qui ne sont plus disponibles barrés et écrits de rouge. Ce choix est
désactivé (il n'est plus sélectionnable).

<input type="radio" disabled>&nbsp;<span style="color: red; text-decoration: line-through">&lt;choix 1&gt;</span><br>
<input type="radio">&nbsp;&lt;choix 2&gt;<br>
<input type="radio">&nbsp;&lt;choix 3&gt;

### Direction / Service

Choix parmi les directions / services de la mairie et du C.C.A.S.

### Champ avec une expression rationnelle
