---
title: Texte enrichi
summary: Description du fonctionnement d'un champ "Texte enrichi"
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-16
---

Le champ "Texte enrichi" est présent dans plusieurs formulaires de l'application. Il permet de créer des contenus bien
structurés et propose plusieurs possibilités de contenus.

<figure markdown="span">

![Panneau de sélection des items dans un champ Texte enrichi](images/img-richtext-01.png)
  <figcaption>Panneau de sélection des items dans un champ Texte enrichi</figcaption>
</figure>

Les éléments disponibles sont :

+ Des titres **H2**, **H3**, **H4**
+ Des listes numérotées :fontawesome-solid-list-ol:
+ Des listes à puce :fontawesome-solid-list-ul:
+ Les liens :fontawesome-solid-link:
+ Des [images](Images.md) :fontawesome-solid-image:
+ Des [documents](Documents.md) :fontawesome-solid-file-pdf:
+ Des [vidéos](Blocks.md#vidéo) :fontawesome-solid-circle-play:

## Ajouter un titre

Au début de la ligne, cliquer sur le bouton :fontawesome-solid-plus-circle: ou taper '/', le panneau de sélection
s'affiche, permettant de cliquer sur le niveau de titre "**H2**" par exemple.

!!! note

    En cliquant devant un texte existant, il sera transformé en titre.

Une page est toujours de niveau "**H1**", les chiffres dénotent le niveau d'imbrication des titres:

```text
H1 <page>
  H2 <Grand titre 1>
    H3 <Titre intrermédiaire 11>
    H3 <Titre intrermédiaire 12>
    H3 <Titre intrermédiaire 13>
  H2 <Grand titre 2>
    H3 <Titre intrermédiaire 21>
    H3 <Titre intrermédiaire 22>
    H3 <Titre intrermédiaire 23>
```

!!! warning "Attention"

    Le système de gestion des contenus détecte les imbrication incorrectes, et averti les éditeurs d'erreurs d'imbrication.

## Ajouter une liste numéroté

En début de ligne Cliquer sur le bouton

## Ajouter une vidéo

L'ajout de vidéos s'effectue en cliquant sur l'icône ":fontawesome-play-circle: Embed" dans la liste des items.
