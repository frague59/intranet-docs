---
title: Pages
summary: Les pages dans le canal
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-10
---

Les pages sont des éléments affichables complètement dans le site, auxquelles sont attachées des URL.

On peut les faire apparaître dans les menus de l'application et programmer l'apparition dans le site.

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

## Affichage des pages dans les menus

Pour afficher une page dans les menus, aller à l'onglet "**Promotion**" dans la page d'édition de la page, et cocher
"**Faire apparaître dans les menus**"

<figure markdown="span">

![Faire apparaître la page les menus](images/img-pages-01.png)
<figcaption>Faire apparaître la page les menus</figcaption>
</figure>

## Programmation de l'affichage des pages

Pour afficher l'état d'une page, cliquer sur le bouton :fontawesome-solid-info-circle: en haut à droite de la page.
d'édition.
<figure markdown="span">

Depuis le lien "**Programmer**", il est possible de définir la programmation d'affichage et de retrait d'une page.

![État d'une page](images/img-pages-02.png)
<figcaption>État d'une page</figcaption>
</figure>

<figure markdown="span">

![Programmation de la visibilité d'une page](images/img-pages-03.png)
<figcaption>Programmation de la visibilité d'une page</figcaption>
</figure>

Il faut alors choisir les dates de publication et de retrait de la page.

<figure markdown="span">

![La programmation a été enregistrée](images/img-pages-04.png)
<figcaption>La programmation a été enregistrée</figcaption>
</figure>

!!! warning "Attention"

    L'affichage d'une page est déclenché par un script qui est executé toutes les heures.

    Il est donc possible que l'horaire d'affichage ne soit pas tout à fait respecté, selon l'heure le déclenchement
    du script.
