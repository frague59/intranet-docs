---
title: Corps de texte
summary: Description et utilisation du cops de texte.
authors:
  - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-18
---

Le corps de texte est un élément complexe, qui permet de saisir un contenu de façon structurée.

Le corps de texte utilise des [blocks](Blocks.md) pour son affichage.

<figure markdown="span">

![Panneau de sélection des items dans un champ StreamField](images/img-streamfield-01.png)
  <figcaption>Panneau de sélection des items dans un champ Texte enrichi</figcaption>
</figure>

+ :fontawesome-solid-paragraph: [Paragrphe](./RichText.md)
+ :fontawesome-solid-table: [Table](./blocks/Table.md)

+ Des [images](Images.md) :fontawesome-solid-image:
+ Des [documents](Documents.md) :fontawesome-solid-file-pdf:
+ Des [vidéos](Blocks.md#vidéo) :fontawesome-solid-circle-play:
+ Des [Postes vacants](Blocks.md#postes-vacants) :fontawesome-solid-user-tie:
+ Des [Groupes d'accordéons pour les documents](Blocks.md#groupe-daccordeons-pour-des-documents) :fontawesome-solid-folder-tree:
+ Des [chiffres-clés](Blocks.md#chiffre-clé) :fontawesome-solid-chart-line:
+ Des [liste de chiffres-clés](Blocks.md#liste-de-chiffre-clé) :fontawesome-solid-list:
