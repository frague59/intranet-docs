---
title: Interface d'administration
summary: Description dfe l'interface d'admiistration du canal
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

## Accéder à l'administration

### Depuis une page

Pour accéder à l'administration du canal, il faut cliquer sur l'oiseau en bas à droite de la page.

<figure markdown="span">

![L'oiseau Wagtail apparaît en bas, à droite de toutes les pages](images/img-administration-01.png)
  <figcaption>L'oiseau Wagtail apparaît en bas, à droite de toutes les pages</figcaption>
</figure>

Un menu s'ouvre alors :

<figure markdown="span">

![Accéder à l'administration du canal](images/img-administration-02.png)
  <figcaption>Accéder à l'administration du canal</figcaption>
</figure>

Ce menu permet de faire plusieurs choses (en fonction des permissions de l'utilisateur):

+ **Aller à l'administration de Wagtail** : Aller à l'administration globale du canal
+ **Montrer dans l'explorateur** : Voir la page courante dans l'explorateur de pages
+ **Modifier cette page** : Modifier la page courante
+ **Ajouter une sous-page** : Ajouter une page sous cette page, si c'est possible et permis

Un indicateur supplémentaire concernant l'accessibilité de la page courante permet de vérifier la structure de la page
courante.

!!! warning "Attention"

    La présentation du menu Wagtail peut varier en fonction des permissions de l'utilisateur et du contexte de la page.

### De manière générale

Il est toujours possible d'accéder à l'administration du canal en tapant l'adresse suivante dans la barre d'adresse du
navigateur :

[https://lecanal.tourcoing.fr/admin/](https://lecanal.tourcoing.fr/admin/)

## L'accueil de l'administration

!!! warning "Attention"

    L'affichage de la page d'accueil peut varier en fonction des permissions de l'utilisateur

<figure markdown="span">

![Vue de la page d'accueil de l'administration](images/img-administration-03.png)
  <figcaption>Vue de la page d'accueil de l'administration</figcaption>
</figure>

L'administration présenta un menu (à gauche) et les dernières modifications effectuées par l'utilisateur connecté (au
centre).

### Menu

+ Recherche : Moteur de recherche dans les pages du site. La recherche s'effectue sur le titre des pages
+ Pages : Permet de naviguer dans les pages, de façon arborescente
+ [Images](Images.md)
+ [Documents](Documents.md)
+ Contacts
  + [Personnes](../contact/Person.md)
  + [Directions / Services](../contact/Hierarchy.md)
+ [Problèmes informatiques](../computing/index.md)
+ [Postes vacants](../human_resource/EmploymentOffers.md)
+ [Restauration](../restauration/index.md)
+ [Plats](../restauration/Plats.md)
  + [Accompagnements](../restauration/Accompagnements.md)
  + [Menus](../restauration/Menus.md)
+ [Évènements et localisations](../calendar_scraper/index.md)
  + [Localisations](../calendar_scraper/Locations.md)
  + [Évènements](../calendar_scraper/Events.md)
+ [Rapports](Reports.md)
+ [Configuration](Configuration.md)
+ Aide
