---
title: Blocks pour les corps de texte
summary: Liste des types de blocks utilisables dans le canal
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-23
---
