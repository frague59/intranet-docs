---
title: Block de liste de pages associées
summary: Block de liste de pages associées
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Affiche une liste de [pages](../Pages.md) associées, sous forme de carte.

## Champs

| Champ | Type                         | Description                           |
|-------|------------------------------|---------------------------------------|
| Titre | Texte simple **obligatoire** | Titre du groupe de pages              |
| Pages | Lise de [pages](../Pages.md) | Pages à associer à l'article courant. |
