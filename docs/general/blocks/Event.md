---
title: Block d'événement
summary: Vlock d'événement
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Permet d'afficher un [événement](../../calendar_scraper/Events.md) interne.

<figure markdown="span">

![Création d'un block "Évènement"](images/img-block-event-01.png)
<figcaption>Création d'un block "Évènement"</figcaption>
</figure>

## Champs d'un évènement

| Champ     | Type                                                            | Description          |
|-----------|-----------------------------------------------------------------|----------------------|
| Évènement | [Évènement](../../calendar_scraper/Events.md) (**obligatoire**) | Évènement à afficher |

## Affichage d'en évènement

<figure markdown="span">

![Affichage d'un block "Évènement"](images/img-block-event-02.png)
<figcaption>Affichage d'un block "Évènement"</figcaption>
</figure>

<figure markdown="span">

![Popup sur l'événement](images/img-block-event-03.png)
<figcaption>Popup sur l'événement</figcaption>
</figure>

!!! warning "Attention"

    L'événement doit être préalablement créé pour être trouvé dans la liste de selection.
