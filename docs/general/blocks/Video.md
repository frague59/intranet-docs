---
title: Block de vidéo
summary: Block de vidéo
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Le block "Vidéo" permet d'insérer une vidéo dans le site. Ces vidéos peuvent provenir de plusieurs sources :

+ [Photothèque de Tourcoing](https://media.tourcoing.fr)
+ [YouTube](https://www.youtube.com)
+ [Vimeo](https://www.vimeo.com)
+ [DailyMotion](https://www.dailymotion.com)
+ [Flickr](https://www.flickr.com/)
+ [Pinterest](https://www.pinterest.fr/)
+ [DeviantArt](https://www.deviantart.com/)

<figure markdown="span">

![Création d'un block de "Vidéo"](images/img-block-video-01.png)
<figcaption>Création d'un block "Vidéo"</figcaption>
</figure>

## Création d'un block vidéo

Pour créer un block vidéo, il faut récupérer cette adresse depuis le site concerné.

### Depuis la photothèque

Rechercher le vidéo puis cliquer sur le menu "**...**" situé en bas à droite de la vignette de la vidéo, puis cliquer
sur "Partager", un panneau s'ouvre.

<figure markdown="span">

![Partager une vidéo depuis la photothèque](images/img-block-video-02.png)
  <figcaption>Partager une vidéo depuis la photothèque</figcaption>
</figure>

<figure markdown="span">

![Panneau de partage](images/img-block-video-03.png)
  <figcaption>Panneau de partage</figcaption>
</figure>

Dans ce panneau, sous le champ "**URL de la vue détaillée**", cliquer sur "**Copier dans le presse-papier**"

Dans le canal, coller l'adresse dans le champ **Vidéo**.

### Depuis un autre site

Pour récupérer une vidéo depuis un autre site, rechercher une bouton "**Partager**" sur la vidéo ou à défaut copier
l'adresse de la page

## Affichage d'une vidéo

<figure markdown="span">

![Affichage d'une vidéo](images/img-block-video-04.png)
<figcaption>Affichage d'une vidéo</figcaption>
</figure>
