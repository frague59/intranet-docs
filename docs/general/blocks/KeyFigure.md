---
title: Block de chiffre-clé
summary: Block de chiffre-clé
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Les chiffres-clés permettent de mettre en avant des chiffres.

<figure markdown="span">

![Affichage un chiffre-clé](images/img-block-key-figure-01.png)
<figcaption>Affichage un chiffre-clé</figcaption>
</figure>

## Création d'un chiffre-clé

<figure markdown="span">

![Création d'un chiffre-clé](images/img-block-key-figure-02.png)
<figcaption>Création d'un chiffre-clé</figcaption>
</figure>

### Champs d'un chiffre-clé

| Champ              | Type                                                                                                        | Description                                                               |
|--------------------|-------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| Titre              | Texte **obligatoire**                                                                                       | Titre du chiffre-clé                                                      |
| Pictogramme        | Icône **obligatoire**                                                                                       | Icône du chiffre-clé                                                      |
| Couleur de l'icône | Choix de couleur, parmi [`Primaire`, `secondaire`, `Marque`, `Info`, `Attention`, `Danger`] **obligatoire** | Ce sont les couleurs définies pour le canal - Valeur par défaut: `Marque` |
| Signe              | Choix du signe, parmi [`Plus`, `Moins`, `Neutre`] **obligatoire**                                           | Signe du chiffre - Valeur par défaut: `Neutre`                            |
| Nombre             | Nombre entier ou décimal **obligatoire**                                                                    | L'afficha s'affiche au type de nombre.                                    |
| Description        | Texte                                                                                                       | Texte additionnel présenté à droite du chiffre.                           |
| Aide               | Texte                                                                                                       | Texte additionnel présenté dans le tooltip après le titre du chiffre-clé  |
