---
title: Block de document
summary: Block de document
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Permet d'insérer un document comme pièce jointe de la page.

<figure markdown="span">

![Création d'un block "Document"](blocks/images/img-block-document-01.png)
<figcaption>Création d'un block "Document"</figcaption>
</figure>

## Champs d'un document

| Champ       | Type                                       | Description             |
|-------------|--------------------------------------------|-------------------------|
| Titre       | Texte simple (**obligatoire**)             | Titre de l'image        |
| Document    | [Document](Documents.md) (**obligatoire**) | Document à joindre      |
| Description | [Texte enrichi](RichText.md)               | Description du document |

## Affichage du document

### Documents PDF

<figure markdown="span">

![Affichage un block "Document"](blocks/images/img-block-document-02.png)
<figcaption>Création d'un block "Document"</figcaption>
</figure>

Pour les documents au format **PDF**, une **prévisualisation** est possible en popup.

<figure markdown="span">

![Affichage une prévisualisation d'un document PDF](blocks/images/img-block-document-03.png)
<figcaption>Affichage une prévisualisation d'un document PDF</figcaption>
</figure>

### Autres documents

Il n'y a pas de prévisualisation des documents autres que PDF.
