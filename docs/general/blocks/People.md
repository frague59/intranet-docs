---
title: Block de personne
summary: Block de personne
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Affichage d'une personne de l'annuaire.

<figure markdown="span">

![Création d'un block "Personne"](images/img-block-people-01.png)
<figcaption>Création d'un block "personne"</figcaption>
</figure>

## Champs d'une personne

| Champ      | Type                                               | Description                                           |
|------------|----------------------------------------------------|-------------------------------------------------------|
| Rôle       | Texte simple (**obligatoire**)                     | Rôle de ma personne                                   |
| Personne   | [Personne](../contact/Person.md) (**obligatoire**) | Choix parmi les personnes                             |
| Avec photo | Booléen (**Oui**, **Non**)                         | Affiche l'image (si autorisé au niveau de l'annuaire) |

!!! warning "Attention"

    Une personne doit avoir été créée au préalable pour pouvoir l'ajouter.

## Affichage d'une personne

<figure markdown="span">

![Affichage d'un block "Personne" dans le site, avec photo](images/img-block-people-02.png)
<figcaption>Affichage d'un block "personne" dans le site, avec photo</figcaption>
</figure>

<figure markdown="span">

![Affichage d'un block "Personne" dans le site, sans photo](images/img-block-people-03.png)
<figcaption>Affichage d'un block "personne" dans le site, sans photo</figcaption>
</figure>
