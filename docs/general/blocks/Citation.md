---
title: Citation
summary: Block pour les citations
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---


<figure markdown="span">

![Création d'une citation](images/img-block-citation-01.png)
<figcaption>Création d'une citation</figcaption>
</figure>

Une citation est un court texte. Il peut être sourcé (auteur, référence)

## Les champs dans une citation

| Champ     | Type                           | Description              |
|-----------|--------------------------------|--------------------------|
| Texte     | Texte simple (**obligatoire**) | Texte de la citation     |
| Auteur    | Texte simple                   | Texte de la citation     |
| Référence | Texte simple                   | Référence de la citation |

## Affichage d'une citation

<figure markdown="span">

![Affichage d'une citation dans le site public](images/img-block-citation-02.png)
<figcaption>Affichage d'une table dans le site public</figcaption>
</figure>
