---
title: Block de liste de chiffres-clés
summary: Block de liste de chiffres-clés
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Une liste de chiffres-clés permet de mettre sous un même titre plusieurs chiffres-clés.

<figure markdown="span">

![Affichage d'une liste de chiffres-clés](images/img-block-key-figure-list-02.png)
<figcaption>Affichage d'une liste de chiffres-clés</figcaption>
</figure>

## Création d'une liste de chiffres-clés

<figure markdown="span">

![Création d'une liste de chiffres-clés](images/img-block-key-figure-list-01.png)
<figcaption>Création d'une liste de chiffres-clés</figcaption>
</figure>

### Champs d'un chiffre-clé

| Champ         | Type                                 | Description                       |
|---------------|--------------------------------------|-----------------------------------|
| Titre         | Texte **obligatoire**                | Titre du chiffre-clé              |
| Chiffres-clés | Liste de [Chiffre-clé](KeyFigure.md) | Liste de chiffres-clés à afficher |
