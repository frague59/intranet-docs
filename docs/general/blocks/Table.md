---
title: Table
summary: Ajouter une table
authors:
    - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Permet de créer des tableaux affichés dans le canal.

<figure markdown="span">

![Création d'une table](images/img-block-table-01.png)
<figcaption>Création d'une table</figcaption>
</figure>

## Les champs dans une table

| Champ             | Type                       | Description                                               |
|-------------------|----------------------------|-----------------------------------------------------------|
| Ligne d'en-tête   | Booléen (**Oui**, **Non**) | Précise si la ligne d'entête doit être mise en évidence   |
| Colonne d'en-tête | Booléen (**Oui**, **Non**) | Précise si la colonne d'entête doit être mise en évidence |
| Légende           | Texte simple               | Texte à ajouter en haut du tableau                        |

Ensuite, vient le tableau à remplir avec les valeurs désirées.

Il est possible d'ajouter des colonnes / lignes, en utilisant le click-droit fans les cellules du tableau.

## Affichage d'une table

<figure markdown="span">

![Affichage d'une table dans le site public](images/img-block-table-02.png)
<figcaption>Affichage d'une table dans le site public</figcaption>
</figure>
