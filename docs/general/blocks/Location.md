---
title: Block de localisation
summary: Block de localisation
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Permet d'afficher une [localisation](../../calendar_scraper/Locations.md)

<figure markdown="span">

![Création d'un block de localisation](images/img-block-location-01.png)
<figcaption>Création d'un block de localisation</figcaption>
</figure>

## Champs d'un localisation

| Champ        | Type                                                                  | Description             |
|--------------|-----------------------------------------------------------------------|-------------------------|
| Localisation | [Localisation](../../calendar_scraper/Locations.md) (**obligatoire**) | Localisation à afficher |

## Affichage d'une localisation

<figure markdown="span">

![Affichage d'une localisation](images/img-block-location-02.png)
<figcaption>Affichage d'une localisation</figcaption>
</figure>

<figure markdown="span">

![Popup de carte d'une localisation](images/img-block-location-03.png)
<figcaption>Popup de carte d'une localisation</figcaption>
</figure>
