---
title: Block des postes vacants
summary: Block des postes vacants
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---


Affiche un poste vacant dans un [texte enrichi](./RichText.md).

Les postes vacants sont choisis parmi les [Poste vacants](../human_resource/EmploymentOffers.md).

<!-- @todo: Update ! -->
