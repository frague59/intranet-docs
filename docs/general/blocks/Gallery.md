---
title: Block de galerie
summary: Block de galerie
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Affiche une galerie de photos, basée sur une [collection](../Collections.md).

<figure markdown="span">

![Création d'un block de Galerie](images/img-block-gallery-01.png)
<figcaption>Création d'un block "Galerie"</figcaption>
</figure>

## Champs d'une galerie

| Champ   | Type                                                    | Description             |
|---------|---------------------------------------------------------|-------------------------|
| Galerie | [Galerie](../publications/Gallery.md) (**obligatoire**) | Localisation à afficher |

## Affichage d'une galerie

<figure markdown="span">

![Affichage d'une galerie](images/img-block-gallery-02.png)
<figcaption>Affichage d'une galerie</figcaption>
</figure>

<figure markdown="span">

![Vue des images en pleine page, avec navigation, zoom, plein écran et rotation](images/img-block-gallery-03.png)
<figcaption>Vue des images en pleine page, avec navigation, zoom, plein écran et rotation</figcaption>
</figure>
