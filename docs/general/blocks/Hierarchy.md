---
title: Block de direction / service
summary: Block de direction / service
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Affiche une direction / un service.

<figure markdown="span">

![Création d'un block "Direction/ service"](images/img-block-hierarchy-01.png)
<figcaption>Création d'un block "Direction/ Service"</figcaption>
</figure>

## Champs d'une direction / service

| Champ               | Type                                                           | Description                           |
|---------------------|----------------------------------------------------------------|---------------------------------------|
| Rôle                | Texte simple (**obligatoire**)                                 | Rôle de ma personne                   |
| Direction / service | [Direction service](../contact/Hierarchy.md) (**obligatoire**) | Choix parmi les Directions / services |

!!! warning "Attention"

    Une direction / service doit avoir été créée au préalable pour pouvoir l'ajouter.

## Affichage d'une direction / service

<figure markdown="span">

![Affichage d'un block "Direction / service"](images/img-block-hierarchy-02.png)
<figcaption>Affichage d'un block "Direction / service"</figcaption>
</figure>
