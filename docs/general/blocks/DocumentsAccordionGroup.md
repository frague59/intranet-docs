---
title: Block de groupe de documents
summary: Block de groupe de documents
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---


Les groupes d'accordéons pour les [documents](../Documents.md) permettent de faire une rendu simplifié de plusieurs
documents, classés en groupes.

**Par exemple :**

+ Lite de documents
  + 2024
    + Doc 1
    + Doc 2
  + 2023
    + Doc 3
    + Doc 4
  + ...

L'affichage utilise un widget "Accordéon", qui permet de montrer une année en cachant les autres.

## Création du groupe d'accordéons

<!-- @todo: Update ! -->
