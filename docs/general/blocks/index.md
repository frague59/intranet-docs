---
title: "Blocks pour les StreamFields"
summary: "Utilisation des différents types de blocks pour les StreamFields"
authors:
    - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

+ :fontawesome-solid-paragraph: [Texte enrichi](../RichText.md)
+ :fontawesome-solid-table: [Table](Table.md)
+ :fontawesome-solid-quote-left: [Citation](Citation.md)
+ :fontawesome-solid-user: [Personne](People.md)
+ :fontawesome-solid-users: [Direction / Service](Hierarchy.md)
+ :fontawesome-solid-calendar: [Événement](Event.md)
+ :fontawesome-solid-location-pin: [Localisation](Location.md)
+ :fontawesome-solid-images: [Galerie](Gallery.md)
+ :fontawesome-solid-circle-play: [Vidéo](Video.md)

+ :fontawesome-solid-file: [Document](Document.md)
+ :fontawesome-solid-image: [Image](Image.md)
+ :fontawesome-solid-file-lines: [Pages associées](LinkedPageList)
+ :fontawesome-solid-link: [Lien externe](ExternalLink.md)
+ :fontawesome-solid-folder-tree: [Groupe d'accordéons pour les documents](DocumentsAccordionGroup.md)
+ :fontawesome-solid-user-tie: [Poste vacant](EmploymentOffer.md)
+ :fontawesome-solid-chart-line: [Chiffre-clé](KeyFigure.md)
+ :fontawesome-solid-list: [Liste de chiffres-clés](KeyFiguresList.md)
