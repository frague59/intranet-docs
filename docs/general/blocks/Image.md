---
title: Block d'image
summary: Block d'image
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-23
---

Afficher une image dans la page.

## Champs d'une image

| Champ      | Type                                                          | Description                        |
|------------|---------------------------------------------------------------|------------------------------------|
| Titre      | Texte simple (**obligatoire**)                                | Titre de l'image                   |
| Image      | [Image](Images.md) (**obligatoire**)                          | Image à insérer                    |
| Alignement | Choix parmi (**Gauche**, **Centre**, **Droite**, **Remplir**) | Alignement de l'image dans la page |

## Affichage d'une image

### Alignée à gauche

<figure markdown="span">

![Affichage de l'image à gauche](images/img-block-image-02.png)
<figcaption>Affichage de l'image à gauche</figcaption>
</figure>

### Alignée à droite

<figure markdown="span">

![Affichage de l'image au centre](images/img-block-image-03.png)
<figcaption>Affichage de l'image au centre</figcaption>
</figure>

### Alignée au centre

<figure markdown="span">

![Affichage de l'image à droite](images/img-block-image-04.png)
<figcaption>Affichage de l'image à droite</figcaption>
</figure>

### Remplir

<figure markdown="span">

![Affichage de l'image en mode "Remplir"](images/img-block-image-05.png)
<figcaption>Affichage de l'image en mode "Remplir"</figcaption>
</figure>
