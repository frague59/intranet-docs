---
title: Documents
summary: Gestion des documents
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-25
---

Les documents permettent de téléverser des fichiers à consulter par les agents. Ils peuvent être au format PDF,
Microsoft Office ou LibreOffice.

La gestion des documents s'appuie sur les [collections](Collections.md) et les [mots-clés](Tags.md).

## Liste de documents

Pour accéder à la liste de l'ensemble des documents, aller dans le menu d'[administration](Administration.md) et cliquer
sur "**Documents**".

<figure markdown="span">

![Liste des documents](images/img-documents-01.png)
<figcaption>Liste des documents</figcaption>
</figure>

## Création de documents

On peut créer un document soit en l'ajoutant depuis le menu d'[administration](Administration.md), soit directement
depuis les [blocks](Blocks.md#document) des [Corps de texte](StreamField.md)

<figure markdown="span">

![La création propose de téléverser le document](images/img-documents-02.png)
<figcaption>La création propose de téléverser le document</figcaption>
</figure>

!!! warning "Attention"

    Ne pas oublier de sélectionner la collection vers laquelle le document sera téléversé.

Après le téléversement du fichier, un panneau apparaît sous la boîte de téléversement pour proposer de mieux spécifier
le document.

Il est important de mettre à jour le **titre du document** (qui sera affiché dans le site) et les [mots-clés](Tags.md).

<figure markdown="span">

![Informations supplémentaires sur le document](images/img-documents-03.png)
<figcaption>Informations supplémentaires sur le document</figcaption>
</figure>

!!! note

    Vous pouvez ajouter plusieurs documents en même temps, dans ce cas un panneau pas document sera affiché.

### Champs des documents

| Champ      | Type                                           | Description                                                  |
|------------|------------------------------------------------|--------------------------------------------------------------|
| Titre      | Texte simple                                   | Titre du document                                            |
| Collection | [Collection](Collections.md) (**obligatoire**) | Collection dans laquelle téléverser le document              |
| Mots-clés  | [Mots-clés](Tags.md)                           | Mots-clés à ajouter au document, pour faciliter la recherche |

## Modification d'un document

Pour modifier un document, cliquer sur son nom dans la liste de documents.

Il est possible depuis cette vue de remplacer le fichier joint au document par un autre document, qui sera
**automatiquement** mis à jour dans la partie publique du site, sans avoir à modifier les pages.

Dans ce cas, **les liens vers ce document sont également mis à jour**.

## Suppression d'un document

La suppression d'un document s'effectue soit depuis la page de modification ou depuis la page de liste de documents.

Une validation de la suppression, avec les éventuels liens vers ce document dans les pages, est présentée.

## Déplacement des documents

Il est possible de changer la collection de documents par lots, en utilisant la sélection multiple depuis la liste des
documents.
