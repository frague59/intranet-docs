---
title: Collections
summary: Description de la gestion des collections
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-18
---

Les collections sont un moyen d'organiser les images et les documents dans l'application. Il est possible d'utiliser
les [galeries](../publications/Gallery.md) pour afficher l'ensemble des images d'une collection dans la page.

Elles peuvent utiliser le système de [mote-clés](Tags.md) pour améliorer la recherche des images ou des documents.

## Liste des collections

Les collections sont accessibles depuis le menu de l'[administration](../general/Administration.md).

<figure markdown="span">

![Accéder à la liste des collections](images/img-collections-01.png)
<figcaption>Accéder à la liste des collections</figcaption>
</figure>

## Ajouter une collection

Cliquer sur le bouton "**Ajouter une collection**" en haut à droite de la page.

<figure markdown="span">

![Création d'une collection](images/img-collections-02.png)
<figcaption>Création d'une collection</figcaption>
</figure>

### Champs d'une collection

| Champ  | Type                                           | Description                                |
|--------|------------------------------------------------|--------------------------------------------|
| Nom    | Texte simple (**obligatoire**)                 | Nom de la collection à créer               |
| Parent | [Collection](Collections.md) (**obligatoire**) | Collection parent de la collection à créer |

!!! note

    La collection **`ROOT`** est la collection racine de toutes les collections.

## Modifier une collection

Pour modifier une collection, il faut cliquer sur le nom de celle-ci dans la liste des collections.

Les champs de la collection sont ceux présentés [ci-dessus](#champs-dune-collection)

<figure markdown="span">

![Modification d'une collection](images/img-collections-03.png)
<figcaption>Modification d'une collection</figcaption>
</figure>

## Supprimer une collection

!!! danger

    La suppression d'une collection supprimera toutes les images et documents qui sont stockées dedans.

La suppression d'une collection se fait depuis la vue de modification d'une collection, et demande une confirmation
avant la suppression effective.

<figure markdown="span">

![Confirmation de suppression d'une collection](images/img-collections-04.png)
<figcaption>Confirmation de suppression d'une collection</figcaption>
</figure>

## Ajouter des documents ou des images dans une collection

Dans le popup d'ajout d'[image](Images.md) ou de [document](Documents.md), sélectionner la collection avant
d'enregistrer l'image ou le [document](Documents.md).
