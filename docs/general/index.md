---
title: Informations générales sur l'édition
summary: Informations générales sur l'édition
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-18
---

Ces pages regroupent les éléments globaux à toute l'administration du canal.

## Les actions globales sur les pages

### La visibilité des pages

Il est possible de programmer la visibilité des pages, en utilisant le bouton :fontawesome-solid-info-cirle: (État) en
haut à droite de la page d'administration de la page.

<figure markdown="span">

![Modifier la programmation d'une page](images/img-general-programmation-01.png)
  <figcaption>Modifier la programmation d'une page</figcaption>
</figure>

Pour programmer la publication, cliquer sur le lien "Programmer" et saisir les dates/heures de début et de fin de
publication.

<figure markdown="span">

![Saisir les dates de début et de fin de publication](images/img-general-programmation-02.png)
  <figcaption>Saisir les dates de début et de fin de publication</figcaption>
</figure>

### La pré-visualisation

Voir l'[article dédié](Previews.md)

### Les commentaires

Il est possible de mettre des commentaires dans les contenus des pages. Ils ne sont pas visibles par les utilisateurs,
mais par les autres éditeurs, pour indiquer par exemple une partie à réécrire par exemple.

#### Ajouter un commentaire

Pour ajouter un commentaire, il faut cliquer sur l'icône :fontawesome-solid-comment: au niveau de l'élément du
formulaire que l'on souhaite commenter. Cette icône apparaît quand on passe la souris sur le champs qu'on souhaite
commenter.

<figure markdown="span">

![Ajouter un commentaire sur un champ - Ouvrir la boîte de dialogue](images/img-general-comment-01.png)
  <figcaption>Ajouter un commentaire sur un champ - Ouvrir la boîte de dialogue</figcaption>
</figure>

<figure markdown="span">

![Ajouter un commentaire sur un champ - Éditer le texte](images/img-general-comment-02.png)
  <figcaption>Ajouter un commentaire sur un champ - Éditer le texte</figcaption>
</figure>

Le **créateur** du commentaire est **identifié**, le commentaire est **daté**.

Il est possible de notifier l'auteur une page de la création d'un commentaire.

#### Visualiser les commentaires

Pour visualiser les commentaires, il faut cliquer sur l'icône :fontawesome-solid-comment-dots: en haut à droite de la
page d'édition.

<figure markdown="span">

![Visualiser les commentaires dans la page](images/img-general-comment-03.png)
  <figcaption>Visualiser les commentaires dans la page</figcaption>
</figure>

## Les types de pages

Il est important de bien choisir le type d'une page au moment de sa création : en effet, selon ce type de page, il sera
possible de faire des choses différentes.

### Article

Un article est une page de contenu, elle n'a pas d'enfants (sous-pages).

Voir [Article](../publications/Articles.md)

### Liste d'articles

Il s'agit d'une page "carrefour", qui affiche des articles (ou des listes d'articles) et des pages de formulaires sous
forme de cartes.

Voir [Liste d'articles](../publications/ArticleLists.md)

### Actualité

Une actualité est destinée à être affichée dans la page d'accueil et d'être affichée complètement.
