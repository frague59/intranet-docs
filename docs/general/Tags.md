---
title: Mots-clés
summary: Gestion des mots-clés
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-25
---

Les mots-clés, présents sur les [images](Images.md) et les [documents](Documents.md), sont un élément important,
permettant d'améliorer la recherche de ces éléments.

!!! warning "Attention"

    Les mots-clés sont facile à créer, mais il est possible de créer des doublons facilement, par exemple "Syndicat" et
    "Syndicats".

## Utiliser les mots-clés pour les images et les documents

<figure markdown="span">

![Les mots-clés sont utilisés pour la sélection rapide des images ou des documents](images/img-tags-01.png)
<figcaption>Les mots-clés sont utilisés pour la sélection rapide des images ou des documents</figcaption>
</figure>

En cliquant sur le mot-clé, les images ayant ce mot-clé sont affichées dans le panneau de sélection de l'image.

## Ajouter un mot-clé dans une image

Pour ajouter un mot-clé à une image, il suffit de cliquer dans la boîte "Mots-Clés" dans la page d'édition de l'image,
et de saisir le mot-clé.

<figure markdown="span">

![Ajout de mots-clés à une image](images/img-tags-02.png)
<figcaption>Ajout de mots-clés à une image</figcaption>
</figure>

## Ajouter un mot-clé dans un document

Pour ajouter un mot-clé à un document, il suffit de cliquer dans la boîte "Mots-Clés" dans la page d'édition de l'image,
et de saisir le mot-clé.

<figure markdown="span">

![Ajout de mots-clés à un document](images/img-tags-03.png)
<figcaption>Ajout de mots-clés à un document</figcaption>
</figure>
