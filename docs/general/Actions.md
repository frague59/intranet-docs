---
title: Actions
summary: Led différents actions applicable à une page.
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-19
---

Des actions peuvent être effectuées "globalement" dans une page.

<figure markdown="span">

![Liste des actions sur une page](images/img-actions-01.png)
  <figcaption>Liste des actions sur une page</figcaption>
</figure>

!!! note

    Selon le type de page considéré, la liste des actions possibles peut changer.

## Modifier

Permet de basculer sur la vue de modification de la page courante.

## Déplacer

Déplace la page courante à un autre emplacement de l'arborescence.

## Copier

Copie la page courante, en proposant de la renommer.

## Supprimer

Supprime la page courante, en proposant une validation avant la suppression effective.

## Publier

Publie la page courante - elle sera **visible** des utilisateurs.

## Dépublier

Dépublie la page courante - elle se sera **plus visible** des utilisateurs.

## Historique

Bascule sur la vue d'historique d'une page, pour voir l'ensemble des actions qui ont étés effectuées dessus.

<figure markdown="span">

![Historique d'une page](images/img-actions-history-01.png)
  <figcaption>Historique d'une page</figcaption>
</figure>

## Ordre de tri des sous-pages

!!! note

    Cette action n'est disponible que pour les pages ayant des sous-pages.

<figure markdown="span">

![Gestion de l'ordre de tri des sous-pages](images/img-actions-ordering-01.png)
  <figcaption>Gestion de l'ordre de tri des sous-pages</figcaption>
</figure>

Pour modifier l'ordre de tri, il suffit de cliquer sur le bouton :fontawesome-solid-grip:{ .rotate-90 } à gauche du
titre de la page et de le déplacer en laissant le bouton de la souris enfoncé.

Il est possible d'ordonner les sous-pages d'une page grâce à cette action. Les sous-pages seront alors présentées dans
la partie publique du site en **suivant cet ordre**.
