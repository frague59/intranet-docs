---
title: Images
summary: Administration et affichage des images
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-18
---

Les images sont gérées dans l'application dans des [collections](Collections.md). Il est possible d'ajouter des images,
d'en modifier les propriétés, de les déplacer...

La gestion des images s'appuie sur les [collections](Collections.md) et les [mots-clés](Tags.md).

## Redimensionnement des images

Un outil d'automatisation pour le redimensionnement des images a été développé spécifiquement :

<http://images.tourcoing.fr>

## Liste des images

Pour accéder à la liste de l'ensemble des images, aller dans le menu d'[administration](Administration.md) et cliquer
sur "**Images**".

<figure markdown="span">

![Liste des images](images/img-images-01.png)
<figcaption>Liste des images</figcaption>
</figure>

## Vocabulaire

+ **Dimension d'une image** : La largeur et la hauteur de l'image, exprimée en pixels.
  Par exemple: `1280x1024` (<largeur>x<hauteur>)
+ **Ratio d'une image** : défini comme le ratio entre la largeur et la hauteur d'une image.
  `r = largeur / hauteur` Par exemple, une image carrée aura un ratio de **1**.
+ **Le format de l'image** : Système d'encodage de l'image utilisé.
  Par exemple : `PNG`, `JPEG`, `WEBP`, `AVIF`...

## Gestion des images dans le canal

Les images sont automatiquement redimensionnées pour être utilisées dans le canal.

Pour préparer les images : **<http://images.tourcoing.fr/>**

Pour le "recentrage" des images, le canal utilise la notion de "Point d'intérêt" qui permet de l'optimiser.

Toutefois, si des dimensions d'origine de l'image sont insuffisantes, le résultat produit par ce redimensionnement ne
sera pas satisfaisant : crénelage, pixellisation...

En plus du redimensionnement, le canal effectue une "mise au format" des images, qui modifie le **format de l'image**,
pour utiliser en priorité `AVIF` et `WEBP`, qui permettent un meilleur taux de compression de celles-ci, et dont plus de
réactivité dans l'affichage.

## Les dimensions des images

!!! note

    Les dimensions indiquées sont celles affichées dans le canal

### Les **bannières**

| Type de périphérique | Dimensions                         | Commentaire                |
|----------------------|------------------------------------|----------------------------|
| **Téléphone mobile** | `600x160`                          |                            |
| **Tablette**         | `800x200`                          |                            |
| **Ordinateur**       | `1000x320`, `1200x360`, `1400x400` | Selon la taille de l'écran |

### Les **vignettes**

| Type de périphérique | Dimensions                      | Commentaire                |
|----------------------|---------------------------------|----------------------------|
| **Téléphone mobile** | `120x120`                       |                            |
| **Tablette**         | `160x160`                       |                            |
| **Ordinateur**       | `240x240`, `320x320`, `360x360` | Selon la taille de l'écran |

### Les images de galeries

| Type de périphérique | Dimensions                       | Commentaire                |
|----------------------|----------------------------------|----------------------------|
| **Téléphone mobile** | `320x200`                        |                            |
| **Tablette**         | `400x250`                        |                            |
| **Ordinateur**       | `500x350`, `640x400`, `1024x360` | Selon la taille de l'écran |

## Ajout d'une ou plusieurs images dans le canal

### Depuis le système de gestion des images

Cliquer sur le bouton "**Ajouter une image**" en haut, à droite de la page de liste des images.

<figure markdown="span">

![Ajout d'une image](images/img-images-02.png)
  <figcaption>Ajout d'une image</figcaption>
</figure>

!!! warning "Attention"

    Sélectionner la collection avant de lancer l'import pour que toutes les images soient importées dans celle-ci.

Après ajout de l'image, un panneau permet de spécifier les caractéristiques de l'image avant de l'enregistrer
effectivement dans l'application :

| Champ      | Type                                           | Commentaire                               |
|------------|------------------------------------------------|-------------------------------------------|
| Titre      | Texte simple                                   | Extrait automatiquement du nom de l'image |
| Collection | [Collection](Collections.md) (**obligatoire**) |                                           |
| Mots-clés  | Liste de [mots-clés](Tags.md)                  |                                           |

<figure markdown="span">

![Ajout d'une image - Caractéristiques de l'image](images/img-images-03.png)
<figcaption>Ajout d'une image - Caractéristiques de l'image</figcaption>
</figure>

!!! note

    Il est possible de sélectionner plusieurs images depuis l'ordinateur, elles seront ajoutées "par lot", dans la même
    collection. Le panneau de caractéristiques s'affichera pour chaque image.

### Depuis l'édition d'une page

<figure markdown="span">

![Depuis la page, il est possible de sélectionner une image ou d'en ajouter une.](images/img-images-04.png)
  <figcaption>Depuis la page, il est possible de sélectionner une image ou d'en ajouter une.</figcaption>
</figure>

<figure markdown="span">

![Panneau de transfert d'image](images/img-images-05.png)
<figcaption>Panneau de transfert d'image</figcaption>
</figure>

| Champ      | Type                                           | Commentaire                               |
|------------|------------------------------------------------|-------------------------------------------|
| Titre      | Texte simple                                   | Extrait automatiquement du nom de l'image |
| Fichier    | Fichier image (**obligatoire**)                |                                           |
| Collection | [Collection](Collections.md) (**obligatoire**) |                                           |
| Mots-clés  | Liste de [mots-clés](Tags.md)                  |                                           |

## Modification d'une image

<figure markdown="span">

![Modification d'une image](images%2Fimg-images-08.png)
  <figcaption>Modification de transfert d'image</figcaption>
</figure>

Il est possible depuis cette vue de modifier :

| Champ      | Type                                           | Commentaire                               |
|------------|------------------------------------------------|-------------------------------------------|
| Titre      | Texte simple                                   | Extrait automatiquement du nom de l'image |
| Fichier    | Fichier image (**obligatoire**)                |                                           |
| Collection | [Collection](Collections.md) (**obligatoire**) |                                           |
| Mots-clés  | Liste de [mots-clés](Tags.md)                  |                                           |

Il est aussi possible de définir le "**Point d'intérêt**" de l'image, qui permettra un redimensionnement intelligent de
celle-ci.

!!! note

    La modification du fichier image mettra à jour toutes les instances de cette image dans les pages.

## Suppression des images

Depuis la vue de liste des images, il est possible de supprimer une ou plusieurs images.

<figure markdown="span">

![Suppression par lot](images/img-images-06.png)
<figcaption>Suppression par lot</figcaption>
</figure>

Il est également possible de supprimer une image depuis la vue de détail d'une image.

La suppression demandera toujours une confirmation avant la suppression définitive. De plus, si une image est utilisé
dans une page, cette information sera présentée dans la page de confirmation.

<figure markdown="span">

![Confirmation de suppression](images/img-images-07.png)
<figcaption>Confirmation de suppression</figcaption>
</figure>

## Déplacement des images

Il est possible de changer la collection d'images par lots, en utilisant la sélection multiple.
