---
title: Icônes
summary: Affichage des icônes
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-23
---

Les icônes de l'application utilisent la librairie d'
icône [FontAwesome 5](https://fontawesome.com/v5/search?o=r&m=free&s=solid).

<figure markdown="span">

  ![FontAwesome](images/img-icons-01.png)
  <figcaption>FontAwesome</figcaption>
</figure>

## Sélectionneur d'icône

Certains composants proposent de sélectionner une icône à l'aide d'un champ de recherche.

Pour trouver une icône, il faut commencer à taper son nom, la liste d'icônes se réduit au fur et à mesure. Pour
sélectionner une icône, il suffit de cliquer dessus + le nom de l'icône apparaît alors dans le champ de recherche.

<figure markdown="span">

  ![Sélectionner une icône](images/img-icons-02.gif)
  <figcaption>Sélectionner une icône</figcaption>
</figure>
