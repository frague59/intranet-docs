---
title: Prévisualiser un élément
summary: La prévisualisation des pages et d'éléments dans l'administration de Wagtail
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-24
---

La plupart des composants du site sont pré-visualisables dans l'interface d'administration de Wagtail. Celà signifie
qu'il est possible de voir à quoi ressemblera le composant quand il sera publié.

<figure markdown="span">

![Prévisualisation d'un article](images/img-previews-00.png)
<figcaption>Prévisualisation d'un article</figcaption>
</figure>

!!! warning "Attention"

    La prévisualisation d'un composant n'est pas toujours parfaite, elle dépend fortement du contexte dans laquelle
    elle est affichée.

## Mon composant est-il pré-visualisable ?

Pour savoir si un composant est pré-visualisable, voir en haut à droite de la page de création et d'édition du
composant, et chercher l'icône :fontawesome-solid-mobile-screen:. Si elle est présente, ça signifie que le composant est
pré-visualisable.

<figure markdown="span">

![Le composant est pré-visualisable dans l'administration.](images/img-previews-01.png)
<figcaption>Le composant est pré-visualisable dans l'administration.</figcaption>
</figure>

## Les formats d'affichage

Les composants peuvent être pré-visualisés pour différents types de périphérique, sélectionnables en haut du panneau de
pré-visualisation.

<figure markdown="span">

![Les différents formats d'affichage](images/img-previews-03.png)
<figcaption>Les différents formats d'affichage</figcaption>
</figure>

Les formats disponibles sont :

+ :fontawesome-solid-mobile-screen: Téléphones mobiles
+ :fontawesome-solid-tablet-screen-button: Tablettes
+ :fontawesome-solid-desktop: Ordinateurs de bureau ou portables
+ :custom-external-link-alt: Ouvrir la pré-visualisation dans un nouvel onglet

## Les modes de prévisualisation

Les modes de prévisualisation permettent de prévisualiser le composant dans différents contextes : par exemple sous
forme de carte ou sous forme de page.

### Accéder aux modes de pré-visualisation

Si plusieurs modes de pré-visualisation sont disponibles, un select s'affiche en bas à gauche du panneau de
pré-visualisation qui propose ces modes.

Sélectionner un autre mode que celui qui est par défaut basculera la visibilité du composant vers cet autre mode.

<figure markdown="span">

![Différents modes de pré-visualisation sont proposés.](images/img-previews-02.png)
<figcaption>Différents modes de pré-visualisation sont proposés.</figcaption>
</figure>
