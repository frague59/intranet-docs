---
title: Gestion des problèmes informatiques
summary: Description de l'application de gestion des problèmes informatiques
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-17
---

## Administration des problèmes informatiques

L'accès aux problèmes informatique s'effectue dans l'interface d'administration du canal.

Un item "Problèmes informatiques est présent dans le menu à gauche de la page.

:fontawesome-solid-server: Problèmes informatiques

### Liste des problèmes

<figure markdown="span">
  ![Liste des problèmes informatiques en cours](images/img-system-issues-01.png)
  <figcaption>Liste des problèmes informatiques en cours</figcaption>
</figure>

La recherche peut s'effectuer par le titre du problème, et un filtrage en fonction de la gravité et d'état de
résolution.

### Création

<figure markdown="span">
  ![Création d'un problème](images/img-system-issues-02.png)
  <figcaption>Création d'un problème</figcaption>
</figure>

| Champ         | Type                                                  | Description                                                                                   |
|---------------|-------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Titre         | Texte (obligatoire)                                   | Titre descriptif du problème                                                                  |
| Description   | [Texte enrichi](../general/RichText.md) (obligatoire) | Description du problème                                                                       |
| Niveau        | Choix unique (obligatoire)                            | Niveau de gravité du problème, parmi **Critique**, **Haut**, **Moyen**, **Bas**, **Très bas** |
| État          | Choix unique (obligatoire)                            | État de résolution parmi **Ouvert**, **En cours**, **Fermé**                                  |
| Contournement | [Texte enrichi](../general/RichText.md) (obligatoire) | Contournement provisoire le temps de la résolution du problème                                |
| Date de début | Date et heure (obligatoire)                           | Début du problème                                                                             |
| Se termine à  | Date et heure                                         | Fin du problème                                                                               |

Les **opérations** permettent de lister l'ensemble des opérations effectuées pour corriger le problème (le cas échéant).

| Champ       | Type                                                  | Description                     |
|-------------|-------------------------------------------------------|---------------------------------|
| Titre       | Texte (obligatoire)                                   | Titre descriptif de l'opération |
| Description | [Texte enrichi](../general/RichText.md) (obligatoire) | Description de l'opération      |

### Édition

Il est possible de modifier un problème informatique en cliquant sur lon titre depuis la liste. Les champs ci-dessus
sont utilisés.

### Suppression

Pour supprimer un problème informatique :

+ Depuis la liste, cocher la case à cocher à gauche du titre du problème, puis cliquer sur "Supprimer" en bas de la
  page (le bouton apparaît quand on a coché la/les cases).
+ Depuis la vue de modification du poste, cliquer sur la flèche à droite du bouton "Enregistrer" : le bouton "Supprimer"
  apparaît.
  ![Le bouton de suppression apparaît](../human_resource/images/img-employment-offers-04.png)

Dans les deux cas, **une page de validation** permet de confirmer la suppression du problème (dans l'application).
