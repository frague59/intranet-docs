---
title: Postes vacants
summary: "Administration des postes vacants"
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-15
---

## Administration des postes vacants

Pour accéder aux postes vacants, il faut aller dans l'administration de l'application puis cliquer sur "Postes vacants"
dans le menu de gauche.

<figure markdown="span">

![Accéder au menu "Postes vacants"](images/img-employment-offers-01.png)
  <figcaption>Accéder au menu "Postes vacants"</figcaption>
</figure>

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

### Liste des postes vacants

La liste des postes vacants permet la recherche des postes vacants par leurs titres et le filtrage :

+ Par Employeur
+ Par catégories (**A**, **B**, **C**)
+ Par date de début de visibilité

<figure markdown="span">

![Afficher et rechercher dans les "Postes vacants"](images/img-employment-offers-02.png)
  <figcaption>Afficher et rechercher dans les "Postes vacants"</figcaption>
</figure>

## Mise à jour d'un poste vacant

Pour modifier un poste vacant, il fait cliquer sur son titre dans la liste.

<figure markdown="span">

![Modifier un "Poste vacant"](images/img-employment-offers-03.png)
  <figcaption>Modifier un "Poste vacant"</figcaption>
</figure>

Les champs proposés sont :

| Nom du champ  | Type de champs                                        | Description                                                              |
|---------------|-------------------------------------------------------|--------------------------------------------------------------------------|
| Titre         | Texte (obligatoire)                                   | Titre du poste                                                           |
| Catégorie     | Choix unique (obligatoire)                            | Catégorie du poste : **A**, **B**, **C**                                 |
| Employeur     | Choix unique (obligatoire)                            | Employeur : **Mairie** ou **C.C.A.S**                                    |
| Description   | [Texte enrichi](../general/RichText.md) (obligatoire) | Description du poste                                                     |
| Date de début | Date (obligatoire)                                    | Date de début d'affichage de l'offre                                     |
| Date de fin   | Date (obligatoire)                                    | Date de fin d'affichage de l'offre                                       |
| Actif         | Oui / Non (case à cocher)                             | Il est possible de désactiver temporairement une offre en la désactivant |
| Document      | [Document](../general/Documents.md)                   | Il est possible d'ajouter une pièce jointe à l'offre                     |

## Création d'un poste vacant

Pour créer un poste vacant, cliquer sur le bouton "Ajouter un Poste vacant" en haut de la page de liste.

Les champs proposés sont les mêmes que pour la mise à jour du poste, si la date courante est comprise entre les dates de
début et de fin de publication, l'offre est affichée immédiatement.

## Suppression d'un poste vacant

Pour supprimer un poste vacant :

+ Depuis la liste, cocher la case à cocher à gauche du titre de l'offre, puis cliquer sur "Supprimer" en bas de la
  page (le bouton apparaît quand on a coché la/les cases).
+ Depuis la vue de modification du poste, cliquer sur la flèche à droite du bouton "Enregistrer" : le bouton "Supprimer"
  apparaît.
  ![Le bouton de suppression apparaît](images/img-employment-offers-04.png)

Dans les deux cas, **une page de validation** permet de confirmer la suppression de l'offre.
