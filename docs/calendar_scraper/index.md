---
title: Le calendrier des évènements
summary: Gestion du calendrier des évènements
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-19
---

Le calendrier des évènements est le moyen de mettre en avant des évènements ponctuels.

Il est possible de mettre en avant des évènements depuis la [page d'accueil](../publications/HomePage.md).

+ [Évènements](Events.md)
+ [Localisations](Locations.md)
