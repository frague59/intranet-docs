---
title: Gestion des localisations
summary: Administration des localisations
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-19
---

Les localisations permettent de localiser des évènements, et peuvent aussi être utilisées comme block
d'un [Corps de texte](../general/Blocks.md#localisation)

Les localisation sont accessible depuis le menu "**Évènements et Localisations**" puis "**Localisations**".

<figure markdown="span">

![Accéder au menu des localisations](images/img-locations-01.png)
  <figcaption>Accéder au menu des localisations</figcaption>
</figure>

## Liste des localisations

<figure markdown="span">

![Liste des localisations](images/img-locations-02.png)
  <figcaption>Liste des localisations</figcaption>
</figure>

Il est possible de filtrer les localisations par sources ou de les rechercher par leur nom.

## Création d'une localisation

| Champs               | Type                          | Description                                                   |
|----------------------|-------------------------------|---------------------------------------------------------------|
| Nom                  | Texte libre (**obligatoire**) | Nom de la localisation                                        |
| Adresse              | Texte libre (**obligatoire**) | Adresse de la localisation                                    |
| Localisation         | Point sur une carte           | Il est possible de localiser précisément un lieu sur la carte |
| Complément d'adresse | Texte libre                   |                                                               |
| Code postal          | Texte libre                   | Valeur par défaut: **59200**                                  |
| Ville                | Texte libre                   | Valeur par défaut: **TOURCOING**                              |
| Image                | [Image](../general/Images.md) | Image illustrative de la localisation                         |
