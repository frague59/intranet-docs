---
title: Gestion des évènements
summary: Administration des évènements
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-23
---

Les événements sont des manifestations publiques ayant lieu dans une localisation, dans une période de temps donné.

Il existe 2 sortes d'évènements :

+ Les évènements internes
+ Les évènements externes

Les **évènements internes** sont les évènements ajoutés par les agents de la mairie ou du C.C.A.S, directement dans le
canal.

Les **évènements externes** sont récupérés automatiquement quotidiennement, depuis des sources externes :

+ [Agenda du MuBA](https://openagenda.com/fr/muba-eugene-leroy-tourcoing)
+ [Agenda du Conservatoire](https://openagenda.com/fr/conservatoire-a-rayonnement-departemental-de-tourcoing)
+ [Agenda du Tourcoing Jazz](https://openagenda.com/fr/tourcoing-jazz)

Il est possible d'ajouter d'autres sources.

!!! warning "Attention"

    Les évènements externes ne sont pas affichés dans le canal pour le moment !

## Liste des évènements

<figure markdown="span">

![Accéder à la liste des évènements](images/img-events-01.png)
<figcaption>Accéder à la liste des évènements</figcaption>
</figure>

### Recherche et filtrage

<figure markdown="span">

![Liste des évènements avec le filtrage et la recherche](images/img-events-02.png)
<figcaption>Liste des évènements avec le filtrage et la recherche</figcaption>
</figure>

Il est possible de filtrer des évènements sur plusieurs critères :

+ Est externe
+ Catégorie
+ Source (pour les évènements externes)
+ [Localisation](Locations.md)

Il est également possible de rechercher un évènement à partir de son titre.

## Création d'un évènement

Pour créer un nouvel évènement, cliquer sur le bouton "**Ajouter un évènement**" en haut à droite de la page de liste.

<figure markdown="span">

![Page de création d'un évènement](images/img-events-03.png)
<figcaption>Page de création d'un évènement</figcaption>
</figure>

!!! note

    Une pré-visualisation de l'évènement est affichée sur la droite de la page de création, dès qu'il y a assez
    d'information.

### Champs d'un évènement

| Champ                 | Type                                                                | Description                                                    |
|-----------------------|---------------------------------------------------------------------|----------------------------------------------------------------|
| Titre                 | Texte simple (**obligatoire**)                                      | Titre de l'évènement                                           |
| Catégorie             | Choix unique                                                        | Catégorie de publication                                       |
| Visibilité de la page | Choix unique parmi (**Mairie**, **C.C.A.S**, **Mairie et C.C.A.S**) | Public voyant la publication                                   |
| Chapeau               | [Texte enrichi](../general/RichText.md) (**obligatoire**)           | Brève description de l'évènement                               |
| Corps de texte        | [Corps de texte](../general/StreamField.md)                         | Description complète de l'évènement                            |
| Localisation          | [Localisation](Locations.md)                                        | Localisation de l'évènement                                    |
| Autre localisation    | Texte simple                                                        | Localisation de l'évènement (si pas de Localisation spécifiée) |
| URL                   | URL externe                                                         | Adresse de la page de référence pour cet évènement             |
| Page liée             | [Page](../general/Pages.md)                                         | Page de référence pour cet évènement                           |

!!! warning "Attention"

    Les champs `Page liée` et `URL` sont mutuellement exclusilfs. Aucun des deux n'est obligatoire.

!!! warning "Attention"

    Les champs `Localisation` et `Autre localisation` sont mutuellement exclusilfs. L'un ou l'autre est obligatoire.

D'autre part, un évènement peut avoir plusieurs horaires :

| Champ                  | Type                            | Description |
|------------------------|---------------------------------|-------------|
| Date et heure de début | Date et heure (**obligatoire**) |             |
| Date et heure de fin   | Date et heure                   |             |

!!! note

    Il est possible de spécifier plusieurs horaires pour un évènement.
    <figure markdown="span">

    ![Prévisualisation](images/img-events-04.png)
    <figcation>Prévisualisation</figcation>
    </figure>

## Modification d'un évènement

Pour modifier un évènement, il faut cliquer sur le titre de l'évènement pour basculer sur la vue de modification.
Les champs proposés sont les mêmes que pour la création.

## Suppression d'un évènement

!!! warning "Attention"

    Seuls les évènements internes peuvent être supprimés, les évènements externes seront recréés si leur
    [source](CalendarScrapers.md) est active.

### Suppression unitaire

Cliquer sur le '**...**' à droite du nom de l'évènement dans la liste, puis cliquer "**Supprimer**". Une page validation
est proposée, avec les éventuelles utilisations de l'évènement dans le site.

Sinon, la suppression est également possible depuis la page de modification de l'évènement, en cliquant sur la flêche du
bouton d'enregistrement et en cliquant sur "**Supprimer**".

### Suppression par lot

La suppression par lot peut être effectuée en utilisant les cases à cocher à gauche du titre de l'évènement dans la
liste, puis en cliquant sur "**Supprimer**" en bas de la page.

Ce bouton n'apparaît que si des **évènements** sont **sélectionnés**.
