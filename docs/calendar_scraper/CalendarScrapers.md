---
title: Récupérateurs de calendrier
summary: Gestion des récupérateurs de calendrier externe
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-07
---

Les récupérateurs de calendrier permettent depuis l'administration de spécifier comment importer les données depuis des
calendriers externes. Pour le moment, seules les sources de type "**[OpenAgenda](https://openagenda.com/)**" sont
opérationnelles, mais d'autres sources
pourront être implémentées dans le futur, en fonction des besoins.

## Sources disponibles

+ [Agenda du MuBA](https://openagenda.com/fr/muba-eugene-leroy-tourcoing)
+ [Agenda du Conservatoire](https://openagenda.com/fr/conservatoire-a-rayonnement-departemental-de-tourcoing)
+ [Agenda du Tourcoing Jazz](https://openagenda.com/fr/tourcoing-jazz)

## Accéder aus récupérateurs de calendrier

<figure markdown="span">

![Accéder au menu des récupérateurs d'évènements](images/img-calendar-scrapers-01.png)
  <figcaption>Accéder au menu des récupérateurs d'évènements</figcaption>
</figure>

Cliquer dans le menu sur l'item "**Configuration**" puis "**Config du calendrier**" puis "**Récupérateurs de calendrier
**".

<figure markdown="span">

![Liste des récupérateurs de calendrier](images/img-calendar-scrapers-02.png)
  <figcaption>Liste des récupérateurs de calendrier</figcaption>
</figure>

| Champs               | Type                                                                          | Description                                                  |
|----------------------|-------------------------------------------------------------------------------|--------------------------------------------------------------|
| Nom                  | Texte simple (**obligatoire**)                                                | Nom du récupérateur de calendrier                            |
| Description          | [Texte enrichi](../general/RichText.md) (**obligatoire**)                     | Description du récupérateur de calendrier                    |
| Clé d'API            | Texte simple                                                                  | Clé d'authentification                                       |
| Type de récupérateur | Choix unique                                                                  | Pour le moment, seul OpenAgenda est implémenté               |
| Périodicité          | Choix unique (**Quotidien**, **Hebdomadaire**, **Mensuel**) (**obligatoire**) | Périodicité de récupération des données depuis le calendrier |
| Actif                | Oui / Non                                                                     | Si coché, le contenu de l'agenda sera effectivement récupéré |
