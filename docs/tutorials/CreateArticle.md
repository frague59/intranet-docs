---
title: Créer un article
summary: Ajouter une article dans le canal
date: 2024-04-26
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
---

L'article est un élément important dans le canal : il permet d'afficher des informations "statiques", dans le sens où
elles ne sont pas mises à jour très souvent.

!!! warning "Attention"

    Un article **ne peut pas avoir de sous-page** !

## Où ajouter un article ?

Les articles sont organisées de façon arborescente dans le site. La [page d'accueil](../publications/HomePage.md) est le
point d'entrée des utilisateurs dans le site, et est donc à la racine de l'arbre.

```text
Page d'accueil
  + Ma collectivité
    + Le comité des œuvres sociales
    + Le conseil municipal
    + ...
  + Mes demandes
  + Mon guide RH
  + Mon package numérique

```

!!! warning "Attention"

    En fonction de vos permissions, vous de pourez peut-être créer de sous-page.

!!! note

    Il y a 2 modes d'affichage à prendre en compte pour una article :

    + Le mode "**Carte**": Affichage de l'article dans la liste des articles
    + Le mode "**Page**" : Affichage de l'ensemble de la page

## Créer un article

<figure markdown="span">

![Créer un article depuis le front](images/img-create-article-01.png)
<figcaption>Créer un article depuis le front</figcaption>
</figure>

Quand vous avez sélectionné la page parente de cet article, cliquer sur le bouton "Wagtail", et sélectionner "**Ajouter
une sous-page**", puis sélectionner "**Article**" dans la liste de types de pages proposées.

<figure markdown="span">

![Sélectionner "Article" dans la liste](images/img-create-article-02.png)
<figcaption>Sélectionner "Article" dans la liste</figcaption>
</figure>

### La page de création d'un article

<figure markdown="span">

![La page de création d'un article](images/img-create-article-03.png)
<figcaption>La page de création d'un article</figcaption>
</figure>

#### Le titre de la page

Le titre de la page doit être descriptif et refléter le contenu de la page.

#### Le public de la page

En fonction de la collectivité à laquelle est rattaché l'utilisateur, la page sera affichée.

Collectivités possibles :

+ Ville : uniquement visible pour les agents de la mairie
+ C.C.A.S : uniquement visible pour les agents du C.C.A.S
+ Ville ou C.C.A.S : Visible à la fois pour les agents Ville et C.C.A.S

#### Les images

##### La vignette

La vignette est importante, elle est affichée dans la page de liste d'articles parente.

Si elle est omise, une image par défaut est affichée.

##### La bannière

Quand elle est ajoutée, elle sera affichée en haut de la page, quand elle est affichée en mode 'page'.

#### Le chapeau

Le chapeau est le court texte introductif qui est affichée dans la vue "Carte"

## Afficher un article dans le menu principal

Pour qu'un article soit affiché dans les menus (page d'accueil et "Burger-menu" :fontawesome-solid-bars:), il doit
