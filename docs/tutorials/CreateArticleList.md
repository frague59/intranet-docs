---
title: Créer une liste d'articles
summary: Manuel de création d'une liste d'articles
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-10
---

Une liste d'articles permet de regrouper des articles en une lists cohérente.

Il est affiché sous la forme d'un tableau de "cartes" d'articles.

## Créer une liste d'articles

Pour créer une liste d'articles, il faut tout d'abord sélectionner la page qui sera la parente de celle que l'on
souhaite créer.

### Trouver la page parente

Soit depuis le "_Front_", soit depuis l'administration.

Par exemple, on souhaite créer une liste d'articles dans "Mon guide RH".

+ Soit depuis le "_Front_"

<figure markdown="span">

![Créer un article depuis le front](images/img-create-article-01.png)
<figcaption>Créer un article depuis le front</figcaption>
</figure>

+ Soit depuis l'administration

<figure markdown="span">

![Créer un article depuis l'administration](images/img-create-article-list-01.png)
<figcaption>Créer un article depuis l'administration</figcaption>
</figure>

### Sélectionner le type de page à ajouter

<figure markdown="span">

![Créer un article depuis l'administration](images/img-create-article-list-02.png)
<figcaption>Créer un article depuis l'administration</figcaption>
</figure>

### Éditer la page

Remplir les

+ Titre : Le titre de la page -- **Obligatoire**
+ Public de la page -- **Obligatoire**
+ Chapeau : Texte introductif du contenu de laa liste des articles ([Texte enrichi](../general/RichText.md)) --
  **Obligatoire**. Ce texte est affiché dans la vue "Carte".
+ Les images :
  + Vignette : Image affichée dans les listes de cartes ([Vignette](../general/Images.md#les-vignettes)) --
    **Obligatoire**
  + Bannière : Image affichée en haut de la page ([Bannière](../general/Images.md#les-bannières))

## Affichage de la page

### Vue Carte

<figure markdown="span">

![La carte présente la vignette, le titre et le chapeau de la liste d'articles](images/img-create-article-list-03.png)
<figcaption>La carte présente la vignette, le titre et le chapeau de la liste d'articles</figcaption>
</figure>

### Vue Page complète

Affiche la liste des sous-pages, sous forme de cartes.

<figure markdown="span">

![Les cartes des sous-pages de la liste d'articles](images/img-create-article-list-04.png)
<figcaption>Les cartes des sous-pages de la liste d'articles</figcaption>
</figure>

### Ajouter des articles à la liste

[Voir "Créer un article"](CreateArticle.md)
