---
title: Tutoriels
summary: Tutoriels d'utilisation du canal
date: 2024-04-26
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
---

Ces tutoriels sont destinés à la prise en main de l'interface d'administration du canal.

+ [Créer une liste d'articles](CreateArticleList.md)
+ [Créer un article](CreateArticle.md)
+ [Créer un formulaire](CreateFormPage.md)

## Prise en main de l'interface d'administration

Voir la [documentation de l'administration](../general/Administration.md)
