---
title: Liens d'applications
summary: Gestion des applications
authors:
  - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-12
---

Les applications sont des liens pré-définis vers des applications métier proposées aux agents de la mairie et du
C.C.A.S.

Elles sont présentes sur toutes les pages du canal, en haut, sous forme de bandeau.

<figure markdown="span">
![Bandeau d'applications](images/img-applications-01.png)
  <figcaption>Bandeau d'applications.</figcaption>
</figure>

Toutes les applications sont toutefois accessibles en cliquant sur le
bouton ![Voir les autres applications](images/img-applications-02.png)

<figure markdown="span">
  ![Toutes les applications accessibles](images/img-applications-03.png)
  <figcaption>Toutes les applications accessibles</figcaption>
</figure>

## Applications toujours présentes

Les applications suivantes sont toujours présentes dans le bandeau principal :

+ **Messagerie**
+ **Annuaire**
+ **Restauration**

## Sélectionner les applications préférées

3 autres applications sont sélectionnables par les utilisateurs, en fonction de leurs besoins. Pour les modifier,
l'utilisateur clique sur son nom dans la en haut à droite de la page, puis "Profil Utilisateur"

<figure markdown="span">
  ![L'utilisateur peut sélectionner ses applications préférées](images/img-applications-04.png)
  <figcaption>L'utilisateur peut sélectionner ses applications préférées</figcaption>
</figure>

## Administration des liens d'applications

Pour accéder aux liens d'applications, aller dans l'interface d'administration, puis cliquer sur "**Configuration**"
:fontawesome-solid-arrow-right: "**Page d'accueil**" :fontawesome-solid-arrow-right: "**Liens d'applications**"

### Liste des applications

<figure markdown="span">

![Liste des applications](images/img-applications-05.png)
<figcaption>Liste des applications</figcaption>
</figure>

### Éditer une application

!!! note

    Une prévisualisation de l'application est proposée.

<figure markdown="span">

![ Éditer une application](images/img-applications-06.png)
<figcaption> Éditer une application</figcaption>
</figure>

### Champs

| Champ                        | Type                              | Description                                                                                |
|------------------------------|-----------------------------------|--------------------------------------------------------------------------------------------|
| Nom                          | Texte libre (**obligatoire**)     | Nom de l'application                                                                       |
| Description                  | Texte libre                       | Court texte descriptif                                                                     |
| URL                          | URL vers l'application            | URL de l'application                                                                       |
| Icône                        | [Icône](../general/Icons.md)      | Icône de l'application                                                                     |
| Sélectionnable               | Booléen                           | L'application sera sélectionnable par les utilisateur                                      |
| Ouvrir dans un nouvel onglet | Booléen                           | L'application sera ouverte dans un nouvel onglet du navigateur                             |
| Activée                      | Booléen                           | L'application sera présentée au niveau des applications préférées et sur la page d'accueil |
| Accès                        | Modalité d'accès de l'application | Depuis quels environnements l'application sera accessible (**réseau local**, **internet**) |
| Public                       | Public de l'application           | **C.C.A.S**, **Mairie**, **Mairie et C.C.A.S**                                                                |
