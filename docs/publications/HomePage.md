---
title: Page d'accueil
summary: Administration de la page d'accueil du site
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-19
---

La page d'accueil est le point d'entrée des agents dans le site.

## Anatomie de la page d'accueil

<figure markdown="span">

![La page d'accueil du site https://lecanal.tourcoing.fr](images/img-homepage-01.png)
  <figcaption>La page d'accueil du site https://lecanal.tourcoing.fr</figcaption>
</figure>

### Le haut de page

Le haut de page comprend plusieurs éléments :

+ Le logo du site, qui permet également le retour vers la page d'accueil dans les autres pages du site
+ Le moteur de recherche, qui permet de rechercher des contenus dans l'ensemble des pages du site
+ Les [applications](ApplicationLinks.md)

### Le corps de la page

#### Le carousel

Le carousel permet de mettre en avant, avec des illustrations, des informations et actualités.

#### Les actualités

Les [actualités](Actualités.md) sont des éléments d'information temporaires, destinées à être changées régulièrement.

#### Les évènements

Les [évènements](../calendar_scraper/Events.md) sont des éléments ponctuels,

#### Les postes vacants

### Le pied de page

Voir la [configuration du pied de page](../general/Footer.md)
