---
title: Galerie
summary: Gestion des galeries
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-24
---

Les **galeries** sont un système pour afficher l'ensemble des images d'une [collection](../general/Collections.md) dans
une page.

Elles sont utilisables dans une [Corps de texte](../general/StreamField.md).

## Créer une galerie

<figure markdown="span">

![Les galeries sont accessibles depuis le menu de l'administration](images/img-gallery-01.png)
<figcaption>Les galeries sont accessibles depuis le menu de l'administration</figcaption>
</figure>

Cliquer sur "**Ajouter une galerie**" en haut à droite de la page.

<figure markdown="span">

![Formulaire de création d'une galerie](images/img-gallery-02.png)
<figcaption>Formulaire de création d'une galerie</figcaption>
</figure>

### Champs d'une galerie

| Champ      | Type                                                      | Description                                 |
|------------|-----------------------------------------------------------|---------------------------------------------|
| Titre      | Texte simple (**obligatoire**)                            | Titre de la galerie                         |
| Chapeau    | [Texte enrichi](../general/RichText.md)                   | Description de la galerie                   |
| Collection | [Collection](../general/Collections.md) (**obligatoire**) | Collection dont les images seront affichées |
