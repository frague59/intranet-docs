---
title: Actualités
summary: Gestion des actualités
author:
  - François GUÉRIN
date: 2024-04-12
---

Les actualités sont des informations, potentiellement présentes sur la [Page d'accueil](HomePage.md).

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

## Créer une actualité

## Champs d'une actualité

| Champ             | Type                                                  | Description                                          |
|-------------------|-------------------------------------------------------|------------------------------------------------------|
| Titre             | Texte libre (**obligatoire**)                         | Titre de la page                                     |
| Public de la page | Choix unique (**obligatoire**)                        | Entre **Ville**, **C.C.A.S** et **Ville et C.C.A.S** |
| Vignette          | [Image](../general/Images.md) (**obligatoire**)       | Vignette carrée, affichée dans les listes de pages   |
| Bannière          | [Image](../general/Images.md)                         | Bannière, affichée en haut de la page                |
| Crédit de l'image | Texte libre (**obligatoire**)                         | Crédit de l'image                                    |
| Chapeau           | [Texte enrichi](../general/RichText.md) (obligatoire) | Accroche de la page, résume succinctement le contenu |
| Corps             | [Corps de texte](../general/StreamField.md)           | Corps de la page                                     |

## Ajouter une actualité existante dans le page d'accueil

Voir [Page d'accueil](HomePage.md#les-actualites)
