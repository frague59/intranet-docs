---
title: Listes d'articles
summary: Gestion des listes d'articles
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-05-24
---

Les listes d'articles sont des pages "carrefour" qui permettent de regrouper des articles, d'autres listes d'articles,
des formulaires en une seule page. Les listes d'articles sont présentées sous forme de tableau de "carte".

Les sous-page **publiées** de la liste d'articles sont automatiquement ajoutées dans la liste d'articles.

<figure markdown="span">

![Affichage d'une liste d'articles dans le site public](images/img-article-list-01.png)
<figcaption>Affichage d'une liste d'articles dans le site public</figcaption>
</figure>

## Édition d'une liste d'article

### Champs d'une liste d'article

| Champ             | Type                                                          | Description                                          |
|-------------------|---------------------------------------------------------------|------------------------------------------------------|
| Titre             | Texte (**obligatoire**)                                       | Titre de la page                                     |
| Public de la page | Choix (**obligatoire**)                                       | Entre **Ville**, **C.C.A.S** et **Ville et C.C.A.S** |
| Vignette          | [Image](../general/Images.md)                                 | Vignette carrée, affichée dans les listes de pages   |
| Bannière          | [Image](../general/Images.md)                                 | Bannière, affichée en haut de la page                |
| Chapeau           | [Texte enrichi](../general/RichText.md) (**obligatoire**)     | Accroche de la page, résume succinctement le contenu |
