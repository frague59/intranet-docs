---
title: Articles
author:
  - François GUÉRIN
date: 2024-04-12
summary: Gestion des articles
---

## Article

Un article est une page complète présentant des informations. Elle n'a pas de sous-pages.

### Ajouter un article

Pour ajouter un article, aller dans la page parent de celui-ci et cliquer sur "Ajouter une sous page" puis sur "Article"

<figure markdown="span">

![Ajouter une sous-page](images/img-article-01.png)
  <figcaption>Ajouter une sous-page</figcaption>
</figure>

<figure markdown="span">

![Sélectionner "Article"](images/img-article-02.png)
  <figcaption>Sélectionner "Article"</figcaption>
</figure>

### Champs des articles

<figure markdown="span">

![Page de création d'un article](images/img-article-03.png)
  <figcaption>Page de création d'un article</figcaption>
</figure>

| Champ             | Type                                                          | Description                                          |
|-------------------|---------------------------------------------------------------|------------------------------------------------------|
| Titre             | Texte (**obligatoire**)                                       | Titre de la page                                     |
| Public de la page | Choix (**obligatoire**)                                       | Entre **Ville**, **C.C.A.S** et **Ville et C.C.A.S** |
| Vignette          | [Image](../general/Images.md)                                 | Vignette carrée, affichée dans les listes de pages   |
| Bannière          | [Image](../general/Images.md)                                 | Bannière, affichée en haut de la page                |
| Chapeau           | [Texte enrichi](../general/RichText.md) (**obligatoire**)     | Accroche de la page, résume succinctement le contenu |
| Corps             | [Corps de texte](../general/StreamField.md) (**obligatoire**) | Corps de la page                                     |

## Liste d'articles

Une liste d'article est une page regroupant différents articles ou d'autres pages, elle peut être utilisée comme élément
de menu, et s'affiche sous forme de panneau d'articles.

### Champs des listes d'articles

| Champ             | Type                                                      | Description                                          |
|-------------------|-----------------------------------------------------------|------------------------------------------------------|
| Titre             | Texte (**obligatoire**)                                   | Titre de la page                                     |
| Public de la page | Choix (**obligatoire**)                                   | Entre **Ville**, **C.C.A.S** et **Ville et C.C.A.S** |
| Vignette          | [Image](../general/Images.md)                             | Vignette carrée, affichée dans les listes de pages   |
| Bannière          | [Image](../general/Images.md)                             | Bannière, affichée en haut de la page                |
| Chapeau           | [Texte enrichi](../general/RichText.md) (**obligatoire**) | Accroche de la page, résume succinctement le contenu |

!!! note

    Il est possible d'ordonner les pages dans la liste en utilisant
    l'[action "Ordre de tri des sous-pages"](../general/Actions.md#ordre-de-tri-des-sous-pages)

!!! warning "Attention"

    Les listes d'article ne disposent pas de [Corps de texte](../general/StreamField.md) -
    Ne **PAS** choisir ce type de page si on souhaite y mettre du contenu directement !
