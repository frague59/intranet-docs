---
title: "Journaux"
summary: Gestion des journaux dans le canal"
authors:
    - François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-08-22
---

## Ajouter le fichier PDF

Pour ajouter un journal, il faut commencer par téléverser le fichier PDF du journal dans
la [collection](../general/Collections.md)
`Journaaux / [titre]`

**Il faut ensuite créer le Journal dans l'interface d'administration.**

## Créer le journal dans l'administration

Pour créer le journal dans l"administration, cliquer sur "Journal" dans le menu de gauche de l'administration de Wagtail

<figure markdown="span">

![La liste des journaux dns l'administration de Wagtail](images/img-newspapers-01.png)
<figcaption>La liste des journaux dns l'administration de Wagtail</figcaption>
</figure>

Cliquer ensuite sur le bouton **[Ajouter un Journal]** en haut à droite de la page.

<figure markdown="span">

![Ajout du journal dans l'administration](images/img-newspapers-02.png)
<figcaption>Ajout du journal dans l'administration</figcaption>
</figure>

| Champ                                | Type                                   | Description                                                              |
|--------------------------------------|----------------------------------------|--------------------------------------------------------------------------|
| Titre                                | Choix parmi les Titres **obligatoire** | Titre du journal                                                         |
| Numéro                               | Entier **obligatoire**                 | Numéro du journal                                                        |
| Sommaire                             | Texte libre                            | Sommaire du journal                                                      |
| Date de publication                  | Date **obligatoire**                   | Date de publication du journal                                           |
| Fichier                              | Fichier PDF **obligatoire**            | Fichier PDF du journal                                                   |
| Prévisualisation de la première page | Image                                  | Prévisualisation de la première page du journal, générée automatiquement |
| URL de prévisualisation              | URL                                    | URL vers un site externe pour prévisualiser le fichier                   |

## Génération de la prévisualisation

À la création du journal, une prévisualisation de la première page du PDF est **générée automatiquement**.
