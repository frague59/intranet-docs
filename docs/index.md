---
title: Documentation de l'intranet - V2
authors:
    - François GUÉRIN <fguerin@ville-tourcoing.fr>
    - Mélissa CHRISTIAENS <mchristiaens@ville-tourcoing.fr>
summary: Mode d'emploi du canal pour les éditeurs
date: 2024-04-12
---

Cette documentation est à l'usage des éditeurs de l'intranet, pour effectuer les différentes tâches qui leur imcombent.

Elle est découpée en fonction de tâches à y effectuer.

## Le profil utilisateur

Le profil utilisateur est l'objet qui contient l'ensemble des préférences de l'utilisateur.

## Images

Les images sont stockées dans l'application, elles sont stockées dans
des [collections](https://guide.wagtail.org/en-5.2.x/how-to-guides/manage-collections/)

### Format des images

Les images présentées dans le canal sont essentiellement sous 2 formes : les **bannières** et les **vignettes**.
Elles sont ajoutées dans l’onglet « IMAGES » pour la catégorie le concernant via la méthode d’ajout suivante :

+ `C_Nomdusujet` – dans la collection CAROUSEL
+ `V_Nomdusujet` – dans la collection VIGNETTE

### Taille des images

+ Bannière : 1400x400
+ Vignette : 360x360
+ Carousel : 1200×600
+ Galerie : 800x600
+

#### Les bannières

Les bannières sont définies au niveau de la publication, une bannière par défaut est proposée (au niveau de la
configuration du thème). Elles sont affichées quand la page elle-même est affichée.

Les bannières sont au format « rectangulaire », entre 600x160 et 1400x400, le redimensionnement est automatique.

Les bannières utilisent le « centre d’intérêt » de l’image pour optimiser le centrage.

Il n’est pas rare d’utiliser des « bannières » pour la mise en valeur graphique du sujet. Ce format bien souvent utilisé
pour les réseaux sociaux s’adapte aussi à l’usage sur le CANAL. Son format est le suivant:

L’image s’ajoute également dans l’onglet « IMAGES » et est soumis au même règle de classement à savoir:

`B_Nomdusujet`

#### Les vignettes

Les vignettes sont définies au niveau de la publication, une vignette par défaut est proposée (au niveau de la
configuration du thème)

Elles sont affichées quand la page parente de la publication est affichée, sous forme de « carte ».

Les vignettes sont au format « carré», entre 120x120 et 360x360, le redimensionnement est automatique.

Les vignettes utilisent le « centre d’intérêt » de l’image pour optimiser le centrage.

#### Les images d’actualités

Pour l’édition de certains articles, des images complémentaires peuvent être demandées pour illustrer les propos. Dans
ce cas, celles-ci doivent être au format XXX

Celles-ci doivent être enregistrées dans le dossier IMAGES puis « ACTUALITÉS » ainsi que dans le dossier de l’année
correspondante à l’actualité en question. Cette méthode permettra d’alléger au fils des années l’espace dédié aux
images.

Pour l’enregistrement, le classement s’effectue :

+ `AnnéeMois_Nomdusujet`  (exemple : 2023oct_JournéePasserelle)
