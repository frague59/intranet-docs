---
title: Personnes
summary: Administration des personnes depuis l'annuaire
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les personnes sont des agents de la mairie et du C.C.A.S, présents dans l'annuaire.

Ils peuvent être affichés dans la partie publique du canal, notamment sous forme
de [blocks](../general/Blocks.md#personne).

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

## Créer une personne

### Récupérer l'adresse de la personne dans l'annuaire

Ouvrir l'annuaire, et rechercher une personne dedans, puis cliquer sur son nom.

<figure markdown="span">

![Afficher la fiche d'une personne dans l'annuaire](images/img-directory-01.png)
  <figcaption>Afficher la fiche d'une personne dans l'annuaire</figcaption>
</figure>

Cliquer sur "**Détail**" pour voir la fiche complète de la personne.

Copier l'adresse **complète de la page** de l'annuaire, présenta dans la barre du navigateur

<figure markdown="span">

![Récupérer l'adresse de la personne dans l'annuaire depuis la barre d'adresse du navigateur](images/img-directory-02.png)
  <figcaption>Récupérer l'adresse de la personne dans l'annuaire depuis la barre d'adresse du navigateur.</figcaption>
</figure>

### Créer la fiche dans le canal

Aller dans le menu principal, cliquer sur "**Contacts**" puis "**Personne**", cliquer sur "**Ajouter une personne
**" en haut à droite de la page, puis coller l'adresse précédemment copiée dans le champ URL.

<figure markdown="span">

![Coller l'adresse précédemment récupérée dans le champ URL](images/img-people-01.png)
  <figcaption>Coller l'adresse précédemment récupérée dans le champ URL</figcaption>
</figure>
