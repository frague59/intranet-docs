---
title: Contacts
summary: Administration des contacts depuis l'annuaire
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les _contacts_ sont une façon de récupérer des données depuis l'[annuaire](https://annuaire.tourcoing.fr) de ma mairie
et du C.C.A.S.

Les informations récupérées dans le canal ne sont pas copiées : si une info est modifiée dans l'annuaire, elle est
répercutée dans le canal, au temps de cache près.

Si une fiche est supprimée dans l'annuaire, elle n'est pas supprimée dans le canal, un message d'erreur s'affiche dans
la partie privée et dans la partir publique du site.

+ [Personnes](Person.md)
+ [Directions / services](Hierarchy.md)
