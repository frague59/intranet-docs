---
title: Directions / services
summary: Administration des directions / services depuis l'annuaire
params:
  author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-04-22
---

Les directions / services sont des entités hiérarchiques.

Ils peuvent être affichés dans la partie publique du canal, notamment sous forme
de [blocks](../general/Blocks.md#directions-services)

!!! note

    Cet élément peut être [prévisualisé](../general/Previews.md)

## Liste des directions / services existant dans le canal

Depuis l'administration, cliquer dans le menu sur "**Contact**" :fontawesome-solid-arrow-right: "**Direction / S
service**"

<figure markdown="span">

![Liste des directions / services](images/img-hierarchy-01.png)
  <figcaption>Liste des directions / services</figcaption>
</figure>

## Détail d'une direction / d'un service

<figure markdown="span">

![Détail de la fiche d'une direction / d'un service](images/img-hierarchy-02.png)
  <figcaption>Détail de la fiche d'une direction / d'un service</figcaption>
</figure>

## Créer une direction / service

### Récupération de l'adresse de la direction / du service depuis l'annuaire

Ouvrir l'annuaire, et rechercher une direction / un service dedans, puis cliquer sur son nom.

<figure markdown="span">

![Afficher la fiche d'une direction / d'un service dans l'annuaire](images/img-directory-03.png)
  <figcaption>Afficher la fiche d'une direction / d'un service dans l'annuaire</figcaption>
</figure>

<figure markdown="span">

![Récupérer l'adresse de la direction / du service dans l'annuaire depuis la barre d'adresse du navigateur](images/img-directory-04.png)
  <figcaption>
    Récupérer l'adresse de la direction / du service dans l'annuaire depuis la barre d'adresse du navigateur.
  </figcaption>
</figure>

### Créer la fiche dans le canal

Depuis l'administration, cliquer dans le menu sur "**Contact**" :fontawesome-solid-arrow-right: "**Direction / S
service**", puis cliquer sur "**Ajouter une Direction / service**" en haut à droite de la page.
