---
title: Formation des utilisateurs
summary: Formation donnée aux utilisateurs dans le cadre de la mise en place di nouveau canal
params:
    author: François GUÉRIN <fguerin@ville-tourcoing.fr>
date: 2024-06-21
---

LE CANAL
Tutoriel d’utilisation

## Déposer une image

1. Cliquer sur l’onglet « IMAGES » dans le menu à gauche de l’écran
2. Demander la création de la collection si elle n’existe pas déjà
3. Cliquer sur « Ajouter une image » en haut à droite sur le carré vert « + »
   Les formats supportés sont les suivants : AVIF, GIF, JPEG, JPG, PNG ou WEBP.
   La taille maximum est de 10,0 Mo.
    1. Glisser et déposer l’image à ajouter directement sur la page ou choisissez le fichier depuis votre ordinateur
    2. Choisissez la collection correspondante à votre image
    3. Ajouter les mots clés correspondants pour faciliter la recherche

4. Modifier la zone d’intérêt si besoin

!!! note "Les collections existantes"

    + Images actualités 2024 (une collection par année pour les images correspondantes à des actualités dites «
    temporaires » avec une date de fin.
    + `V-` Vignette : pour les images vignettes de chaque article. Cette image doit être intégrée au format TAILLE
    + `C-` Carrousel : pour les images au format UNE pour le carrousel. Celles-ci doivent être ajoutées au format
      suivant : 1165x544 pixels.
    + `B-` bandeaux actualités : ces images permettent d’intégrer un visuel au sein de votre article. Celles-ci doivent
      respecter le format suivant :

!!! note "Bon à savoir"

    Pour ajouter un mot clé avec des espaces, il faut ajouter des guillemets "..."

!!! warning "ATTENTION AUX DOUBLONS"

    Pensez à vérifier que votre image n’existe pas déjà sur le serveur à l’aide de l’outil recherche de l’onglet. Le but
    étant de ne pas surcharger le serveur.

## Déposer un document

1. Préparer votre document
   L’objectif est de privilégier les PDF remplissables en direct. Les formats WORD, PPT et EXCEL sont acceptés mais ils
   sont
   à éviter au maximum.
   Licence pour tous ? Ou demande de communication ?
2. Renommer le document avant l’intégration sur le site
   Nom_Année > pour l’ensemble des documents
3. Cliquer sur l’onglet « document » dans le menu à gauche
4. Cliquer sur le bouton « Ajouter un document » en haut à droite
5. Intégrer le document dans la bonne collection
6. Ajouter les mots clés correspondants pour faciliter la recherche

!!! note "Bon à savoir"

    Demander la création de votre collection si elle n’existe pas déjà. BEAUCOUP de collections sont déjà
    intégrées. L’objectif n’est pas de trop les multiplier sauf s’il s’agit de documents « temporaires » comme un tableau
    d’avancement ou autre. (Exemple à montrer pour AGPI juin) : c’est plus simple pour tout supprimer d’un coup.

### Pour modifier le document

En cas d’actualisation d’un document par rapport à une année passée, il est nécessaire de modifier le document existant
et non dans créer un nouveau afin que le changement soit fait automatiquement sur toutes les pages liées au document.

## Création d’un article

Bon à savoir : un article concerne une information permanente c’est-à-dire un sujet qui a vocation à rester dans le
temps. Une actualité est plutôt temporaire et concerne un sujet d’actualité. Les articles ne peuvent cependant pas être
mis en UNE car celle-ci ne concerne que les actualités. Les actualités ne sont créées que par la communication interne.
Leur diffusion relève d’un planning de diffusion précis et validé en amont.

1. Cliquer sur l’onglet PAGES puis sur la section qui vous intéresse en fonction de votre sujet en cliquant sur la
   flèche puis sur le crayon.
   MA COLLECTIVITE, MON GUIDE RH, MON PACK NUMERIQUE ou ACTUALITES

2. Cliquer sur « Ajouter une sous-page » en choisissant le type de la page

    + Article > information permanente sur un sujet professionnel
    + Liste d’articles > page carrousel reprenant plusieurs articles
    + Page de formulaire > page d’inscription
    + Liste de problèmes informatiques ?

4. Ajouter le titre court et rapide. Celui-ci doit être attractif et donner envie de cliquer pour en savoir plus. Pas
   plus d’une phrase.
5. Sélectionner le public concerné : Ville et CCAS ou l’un ou l’autre
6. Ajouter l’image en vignette mais aussi en bannière. Les deux sont obligatoires. (Mettre en obligatoire)
7. Ajouter un chapeau - Il s’agit d’une accroche impactant pour donner plus de précisions sur l’article. Celui-ci doit
   être court pour pouvoir être affiché sur la UNE sans être « coupé ». Pensez à vérifier dès la publication.
8. Créer le corps de l’article en cliquant sur le petit plus + en vert
9. Sélectionner l’outil souhaité

+ Paragraphe < texte brut

!!! note "Bon à savoir"

    N’hésitez pas à ajouter quelques émoticônes pour aérer et rendre la lecture plus agréable. Elles sont disponibles
    ici grâce à un simple copié/collé : [https://emojiterra.com/](https://emojiterra.com/)

!!! note "Bon à savoir"

    Celle-ci doit être intégrer avant dans l’onglet « image ». On ne change pas la mention crédit.

Différentes tailles de titres sont à votre disposition pour classer les informations : H2,H3, H4. Vous pouvez également
comme un éditeur de texte : mettre des infos en italique, en gras, les souligner, faire une liste de puces.

### Types de blocks

+ Table < tableau à construire
+ Citation < pour mettre en valeur une prise de parole
+ Personne et Direction/service < reliée à l’annuaire pour donner les coordonnées des agents ou service
+ Evénement > il faut que l’événement soit créé en amont dans l’onglet correspondant pour pouvoir le retrouver ici
+ Localisation > Celle-ci doit également être créé en amont pour pouvoir être ajouter
+ Galerie > Création en amont de la page galerie, de la collection mais aussi de l’ajout des photos au bon format
+ Vidéo > ? Comment on procède ? Ajouter sur la photothèque avant ?
+ Document > à ajouter dans l’onglet « Document » en amont de la publication de l’article
+ Image > Ajouter une image d’illustration pour aérer le texte et le rendre plus attractif
+ Pages associées  > ?

!!! warning "Attention"

    Pour toutes demandes de créations (événement, vidéo, galerie) ou erreur annuaire, veuillez prendre contact avec la
    communication interne afin de demander la création du support.

### Soumettre en validation

!!! warning "Attention"

    Aucun article ne doit être publié. Celui-ci doit être mis en brouillon dans un premier temps si vous ne l’avez pas
    terminé. Il doit également être intégré sur le CANAL uniquement lorsque vous avez obtenu la validation de votre
    hiérarchie. Y compris les élus !
    Pour la création d’une actualité, il faut absolument passer par la demande de communication en ligne. La procédure ne
    change pas même pour un contributeur.

#### Explication du processus

!!! note Bon à savoir

    Durant le processus de création, vous pouvez pré visualiser votre page sur le côté droit de votre écran
    en cliquant sur l’icône ordinateur. Vous pouvez également ouvrir dans un nouvel onglet en cliquant sur la dernière
    icône.
